@extends('layouts.app')

@section('content')
<div class="uk-width-1-1">
    <div :uk-sticky="'offset: 80; bottom: #communication-form'">
        <div class="uk-tile uk-tile-muted uk-padding-small">
            <div class="uk-child-width-1-2 uk-grid-collapse" uk-grid>
                <div>Costo: {{ $communication->getCost() }}</div>
                <ch-api-label
                        class="uk-text-right"
                        url="{{ route('api.credits.balance') }}"
                        format="Credito attuale: %field%"
                ></ch-api-label>
            </div>
        </div>
    </div>
</div>
<h2 class="uk-heading-divider">{{ $communication->title }}</h2>
@include("communications.parts.header.{$communication->state}")
@if($communication->type->template)
    @include("communications.parts.preview.template")
@else
    @include("communications.parts.preview.plain")
@endif
@endsection