@extends('layouts.app')

@section('content')
  <a href="{{ route('communications.create') }}" class="uk-button uk-button-primary uk-margin-bottom">Nuova Comunicazione</a>
    <ch-table
      url="{{ route('api.communications.index') }}"
      :fields="[
        {
          name: 'title',
          sortable: true,
          label: 'Titolo'
        },
        {
          name: 'slot:type',
          label: 'Tipo'
        },
        {
          name: 'slot:created_at',
          sortable: true,
          label: 'Creato il',
          orderField: 'created_at',
        },
        {
          name: 'sent_at',
          sortable: true,
          label: 'Inviato il'
        },
        {
          name: 'slot:actions'
        }
      ]"
      :edit-on-click="false"
    >
      <div slot="actions" slot-scope="props" class="uk-child-width-1-2 uk-grid-collapse" uk-grid>
        <a v-if="props.item.state === 'ready'" class="uk-icon-link uk-text-center" uk-icon="icon: pencil; ratio: 1.5" :href="'communications/'+props.item.id+'/edit'"></a>
        <ch-api-button
          v-if="props.item.state !== 'sent'"
          :url="'api/communications/'+props.item.id+'/send'"
          method="put"
          button-type="link"
          icon="play-circle"
          icon-ratio="1.5"
          confirm-text="Inviare la comunicazione?"
          error-heading="<h3>Errore durante l'invio:</h3>"
          :enabled="props.item.state === 'ready'"
        ></ch-api-button>
        <ch-api-button
          v-else
          class="uk-text-muted"
          :url="'api/communications/'+props.item.id+'/copy'"
          button-type="link"
          icon="copy"
          icon-ratio="1.5"
          confirm-text="Duplicare il contenuto selezionato?"
          error-heading="<h3>Errore durante la copia:</h3>"
        ></ch-api-button>
      </div>
      <div slot="created_at" slot-scope="props" v-text="new Date(props.item.created_at).toLocaleDateString()"></div>
      <div slot="type" slot-scope="props" v-text="(props.item.type) ? props.item.type.name : '-'"></div>
    </ch-table>
@endsection