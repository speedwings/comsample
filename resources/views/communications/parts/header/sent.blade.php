<div uk-grid>
  <div class="uk-width-1-1 uk-width-1-2@s">
    <dl class="uk-description-list uk-description-list-divider">
      <dt>Inviata il</dt>
      <dd>{{ $communication->sent_done_at->formatLocalized('%d/%m/%Y') }}</dd>
      <dt>Destinatari</dt>
      <dd>{{ $communication->recipients_count }}</dd>
    </dl>
  </div>
  <div class="uk-width-1-1 uk-width-1-2@s">
    <ch-api-button
      class="uk-button-large uk-width-1-1"
      url="{{ route('api.communications.copy', $communication->id) }}"
      button-text="Duplica"
      icon="copy"
      confirm-text="Duplicare il contenuto selezionato?"
      error-heading="<h3>Errore durante la copia:</h3>"
    ></ch-api-button>
  </div>
</div>