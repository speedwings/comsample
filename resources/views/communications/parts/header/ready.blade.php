<div uk-grid>
    <div class="uk-width-1-1 uk-width-1-2@s">
        <dl class="uk-description-list uk-description-list-divider">
            <dt>Potenziali Destinatari</dt>
            <dd>{{ $recipients_count }}</dd>
            @if ($communication->sent_error)
                <dt class="uk-text-danger">Errori</dt>
                <dd class="uk-text-danger">{{$communication->sent_error}}</dd>
            @endif
        </dl>
    </div>
    <div class="uk-width-1-1 uk-width-1-2@s">
        <a href="{{ route('communications.edit', $communication->id) }}"
           class="uk-button uk-button-default uk-button-large uk-width-1-1 uk-margin-bottom">
            <i uk-icon="icon: pencil"></i>
            Modifica
        </a>
        <ch-api-button
                class="uk-button-large uk-width-1-1"
                url="{{ route('api.communications.send', $communication->id) }}"
                method="put"
                button-text="{{ (empty($communication->sent_error)) ? 'Invia' : 'Reinvia' }}"
                button-type="primary"
                icon="play-circle"
                confirm-text="Inviare la comunicazione?"
                error-heading="<h3>Errore durante l'invio:</h3>"
                :enabled="{{ $recipients_count }} > 0"
                :redirect-on-success="true"
        ></ch-api-button>
    </div>
</div>