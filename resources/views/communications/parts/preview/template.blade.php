<div uk-grid>
  <div class="uk-width-1-1">
    <p class="uk-margin-bottom">Oggetto: <strong>{{ $communication->subject }}</strong></p>
    <ch-mail-preview
      url="{{ route('news.html', $communication->id) }}"
      :open="{{  ($communication->sent_at === null) ? 'true' : 'false' }}"
    ></ch-mail-preview>
  </div>
</div>