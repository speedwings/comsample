<div uk-grid>  
  <div class="uk-width-1-1">
    <h5 class="uk-heading-line uk-margin-bottom uk-text-muted"><span>Anteprima</span></h5>
    <div class="uk-tile uk-tile-muted">
      {{ $communication->body }}
    </div>
  </div>
</div>