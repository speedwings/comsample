<div id="communication-form" slot-scope="props" v-if="props.item" class="uk-grid uk-grid-small" uk-grid>
    <div class="uk-width-1-1">
        <div :uk-sticky="'offset: 80; bottom: #communication-form'">
            <div class="uk-tile uk-tile-muted uk-padding-small">
                <div class="uk-child-width-1-2 uk-grid-collapse" uk-grid>
                    <div></div>
                    <ch-api-label
                        class="uk-text-right"
                        url="{{ route('api.credits.balance') }}"
                        format="Credito attuale: %field%"
                    ></ch-api-label>
                </div>
            </div>
        </div>
    </div>
    <div class="uk-width-1-1">
        <label class="uk-form-label" for="ch-communication-title">Titolo</label>
        <div class="uk-form-controls">
            <input class="uk-input uk-form-large" id="ch-communication-title" v-model="props.item.title">
        </div>
    </div>
    <div class="uk-width-1-1" v-show="false">
        <label class="uk-form-label">Tipo</label>
        <ch-select
            class="uk-form-controls"
            label="name"
            v-model="props.item.type_id"
            url="/api/communication_types"
            :init="true"
            @select="(val) => props.item.type = val || {}"
        >
        </ch-select>
    </div>
    <div class="uk-width-1-1">
        <ch-segments-manager
            :segment-id="props.item.segment_id"
            :model-filters="props.item.filters"
            select-url="{{ route('api.segments.index') }}"
            count-url="{{ route('api.segments.count') }}"
            :route="props.item.type && props.item.type.route"
            @select="(val) => props.item.segment_id = val"
            @change="(val) => props.item.filters = val"
        ></ch-segments-manager>
    </div>
    <div class="uk-width-1-1" v-if="props.item.type && props.item.type.route === 'email'">
        <label class="uk-form-label" for="ch-communication-subject">Oggetto</label>
        <div class="uk-form-controls">
            <input class="uk-input uk-form-large" id="ch-communication-subject" v-model="props.item.subject">
        </div>
    </div>
    <div class="uk-width-1-1" v-if="props.item.type.template">
        <label class="uk-form-label" for="ch-contact-firstname">Immagine di testata</label>
        <div class="uk-form-controls">
            <ch-uploader
                    id="ch-communication-header"
                    v-model="props.item.header"
                    url="{{ route('api.pictures.store') }}"
                    :params="{
                    width: 500
                  }"
                    :multiple="false"
                    accept=".jpg"
            ></ch-uploader>
        </div>
    </div>
    <div class="uk-width-1-1">
        <label class="uk-form-label" for="ch-contact-firstname">Contenuto</label>
        <div class="uk-form-controls">
            <ch-editor
                v-if="props.item.type.template"
                id="ch-communication-body"
                v-model.sync="props.item.body"
            ></ch-editor>
            <ch-sms-textarea
                v-else
                v-model="props.item.body"
                :invalid.sync="props.item.invalid"
                :gsm-only="true"
            ></ch-sms-textarea>
        </div>
    </div>
</div>