@extends('layouts.app')

@section('content')
  <ch-form
    url="{{ route('api.communications.show', $id) }}"
    close-uri="{{ route('communications.index') }}"
    :show-on-update="true"
  >
    @include('communications.parts.form')
  </ch-form>
@endsection