@extends('layouts.app')

@section('content')
<ch-form
  url="{{ route('api.communications.store') }}"
  :is-new="true"
  :deletable="false"
  :default-data="{type_id: {{ ($type_id) ?? 'null' }},type: {}, segment_id: null}"
  :edit-on-create="false"
>
  @include('communications.parts.form')
</ch-form>
@endsection