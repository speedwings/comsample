@extends('layouts.app')

@section('content')
    <a href="{{ route('contacts.create') }}" class="uk-button uk-button-primary uk-margin-bottom">Nuovo Contatto</a>
    <ch-table
            url="{{ route('api.contacts.index') }}"
            :export-formats="[
        {
          id: 'csv',
          label: 'CSV',
          url: '{{ url('export/contacts/csv') }}'
        },
        {
          id: 'excel',
          label: 'Excel',
          url: '{{ url('export/contacts/xls') }}'
        }
      ]"
            :fields="[
        {
          name: 'firstname',
          sortable: true,
          label: 'Nome'
        },
        {
          name: 'lastname',
          sortable: true,
          label: 'Cognome'
        },
        {
          name: 'birthdate',
          sortable: true,
          label: 'Data di nascita'
        },
        {
          name: 'slot:email',
          sortable: true,
          label: 'Email',
          orderField: 'email'
        },
        {
          name: 'phone',
          label: 'Telefono'
        },
        {
          name: 'slot:subscriptions',
          label: 'Iscrizioni'
        },
        {
          name: 'slot:city',
          orderField: 'city.denominazione_corrente',
          label: 'Città'
        },
        {
          name: 'source',
          sortable: true,
          label: 'Provenienza'
        }
      ]"
    >
        <div slot="filters" slot-scope="props" class="uk-tile uk-tile-muted uk-padding-remove">
            <button class="uk-button uk-button-primary uk-width-1-1" uk-toggle="target: #table-filters">Filtri <i
                        uk-icon="icon: settings"></i></button>
            <div id="table-filters" class="uk-tile uk-padding-small">
                <div uk-grid>
                    <div class="uk-width-1-1 uk-width-1-2@s uk-width-1-6@m">
                        <label class="uk-form-label" for="ch-filter-gender">Sesso</label>
                        <div class="uk-form-controls">
                            <select class="uk-select" id="ch-filter-gender" v-model="props.filters.gender">
                                <option value=""></option>
                                <option value="M">M</option>
                                <option value="F">F</option>
                            </select>
                        </div>
                    </div>
                    <div class="uk-width-1-1 uk-width-1-2@s uk-width-1-6@m">
                        <label class="uk-form-label">Età</label>
                        <div class="uk-grid-small" uk-grid>
                            <div class="uk-width-1-2">
                                <div class="uk-form-controls">
                                    <input type="text" id="ch-filter-age-min" class="uk-input"
                                           v-model.lazy="props.filters.age_min" placeholder="Da"/>
                                </div>
                            </div>
                            <div class="uk-width-1-2">
                                <div class="uk-form-controls">
                                    <input type="text" id="ch-filter-age-max" class="uk-input"
                                           v-model.lazy="props.filters.age_max" placeholder="A"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="uk-width-1-1 uk-width-1-2@s uk-width-1-6@m">
                        <label class="uk-form-label" for="ch-filter-email_state">Email</label>
                        <div class="uk-form-controls">
                            <select class="uk-select" id="ch-filter-email_state" v-model="props.filters.email_state">
                                <option value=""></option>
                                <option value="available">Inserita</option>
                                <option value="empty">Non inserita</option>
                                <option value="confirmed">Confermata</option>
                                <option value="subscribed">Iscritta</option>
                                <option value="unsubscribed">Non iscritta</option>
                            </select>
                        </div>
                    </div>
                    <div class="uk-width-1-1 uk-width-1-2@s uk-width-1-6@m">
                        <label class="uk-form-label" for="ch-filter-phone_state">Telefono</label>
                        <div class="uk-form-controls">
                            <select class="uk-select" id="ch-filter-phone_state" v-model="props.filters.phone_state">
                                <option value=""></option>
                                <option value="available">Inserito</option>
                                <option value="empty">Non inserito</option>
                                <option value="confirmed">Confermato</option>
                                <option value="subscribed">Iscritto</option>
                                <option value="unsubscribed">Non iscritto</option>
                            </select>
                        </div>
                    </div>
                    <div class="uk-width-1-1 uk-width-1-3@m">
                        <label class="uk-form-label" for="ch-filter-source">Provenienza</label>
                        <ch-select
                            class="uk-form-controls"
                            v-model="props.filters.source"
                            :init="true"
                            url="/api/data/contacts/source"
                        >
                        </ch-select>
                    </div>
                    <div class="uk-width-1-1 uk-width-1-3@m">
                        <label class="uk-form-label" for="ch-filter-city">Comune</label>
                        <ch-select
                            class="uk-form-controls"
                            label="denominazione_corrente"
                            v-model="props.filters.city"
                            url="/api/data/comuni"
                        >
                        </ch-select>
                    </div>
                </div>
            </div>
        </div>
        <div slot="subscriptions" slot-scope="props">
            <p v-if="props.item.email_is_subscribed && props.item.phone_is_subscribed">Email, SMS</p>
            <p v-else-if="props.item.email_is_subscribed">Email</p>
            <p v-else-if="props.item.phone_is_subscribed">SMS</p>
            <p v-else>-</p>
        </div>
        <div slot="email" slot-scope="props" v-text="(props.item.email) ? props.item.email : '-'" :class="{ 'uk-text-primary': props.item.email_confirmed_at }"></div>
        <div slot="city" slot-scope="props" v-text="(props.item.city) ? props.item.city.denominazione_corrente : '-'"></div>
    </ch-table>
@endsection