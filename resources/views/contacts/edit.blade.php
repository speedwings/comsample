@extends('layouts.app')

@section('content')
  <ch-form
    url="{{ route('api.contacts.show', $id) }}"
    close-uri="{{ route('contacts.index') }}"
    :success-modal-buttons="{
      'ok_label': 'CONTINUA',
      'cancel_label': 'CHIUDI',
      'cancel_url': '{{ route('contacts.index') }}'
    }"
  >
    @include('contacts.parts.form')
  </ch-form>
@endsection