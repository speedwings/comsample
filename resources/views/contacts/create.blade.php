@extends('layouts.app')

@section('content')
  <ch-form
    url="{{ route('api.contacts.store') }}"
    close-uri="{{ route('contacts.index') }}"
    :is-new="true"
    :deletable="false"
    :success-modal-buttons="{
      'ok_label': 'CONTINUA',
      'cancel_label': 'CHIUDI',
      'ok_url': '{{ route('contacts.create') }}',
      'cancel_url': '{{ route('contacts.index') }}'
    }"
  >
    @include('contacts.parts.form')
  </ch-form>
@endsection