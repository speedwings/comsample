<div slot-scope="props" v-if="props.item" class="uk-grid uk-grid-small" uk-grid>
    <div class="uk-width-1-1 uk-width-2-5@m">
        <label class="uk-form-label" for="ch-contact-firstname">Nome</label>
        <div class="uk-form-controls">
            <input class="uk-input" :class="{ 'uk-form-danger': props.errors.indexOf('firstname') >= 0 }"
                   id="ch-contact-firstname" v-model="props.item.firstname">
        </div>
    </div>
    <div class="uk-width-1-1 uk-width-2-5@m">
        <label class="uk-form-label" for="ch-contact-lastname">Cognome</label>
        <div class="uk-form-controls">
            <input class="uk-input" :class="{ 'uk-form-danger': props.errors.indexOf('lastname') >= 0 }"
                   id="ch-contact-lastname" v-model="props.item.lastname">
        </div>
    </div>
    <div class="uk-width-1-1 uk-width-1-5@m">
        <label class="uk-form-label" for="ch-contact-gender">Sesso</label>
        <div class="uk-form-controls">
            <select class="uk-select" id="ch-contact-gender" v-model="props.item.gender">
                <option value="M">M</option>
                <option value="F">F</option>
            </select>
        </div>
    </div>
    <div class="uk-width-1-1 uk-width-1-2@m">
        <label class="uk-form-label" for="ch-contact-email">Email<span class="uk-align-right uk-margin-remove"><label
                        class="uk-margin-small-right">Iscritto</label><input class="uk-checkbox" type="checkbox"
                                                                             v-model="props.item.email_is_subscribed"/></span></label>
        <div class="uk-inline uk-width-1-1">
            <span v-if="props.item.id && props.item.email && props.item.email_confirmed_at"
                  class="uk-form-icon uk-form-icon-flip uk-text-primary" uk-icon="icon: check"></span>
            <a v-if="props.item.id && props.item.email && !props.item.email_confirmed_at"
               class="uk-form-icon uk-form-icon-flip" uk-icon="icon: clock"
               @click="() => { props.update.message = 'Confermare l\'indirizzo email?'; props.update.data = {'email_is_confirmed': true, 'email': props.item.email} }"></a>
            <input class="uk-input uk-form-disabled" id="ch-contact-email"
                   :class="{ 'uk-form-danger': props.errors.indexOf('email') >= 0 }" v-model="props.item.email">
        </div>
    </div>
    <div class="uk-width-1-1 uk-width-1-2@m">
        <label class="uk-form-label" for="ch-contact-phone">Telefono<span class="uk-align-right uk-margin-remove"><label
                        class="uk-margin-small-right">Iscritto</label><input class="uk-checkbox" type="checkbox"
                                                                             v-model="props.item.phone_is_subscribed"/></span></label>
        <div class="uk-inline uk-width-1-1">
            <span v-if="props.item.id && props.item.phone && props.item.phone_confirmed_at"
                  class="uk-form-icon uk-form-icon-flip uk-text-primary" uk-icon="icon: check"></span>
            <a v-if="props.item.id && props.item.phone && !props.item.phone_confirmed_at"
               class="uk-form-icon uk-form-icon-flip" uk-icon="icon: clock"
               @click="() => { props.update.message = 'Confermare il numero di telefono?'; props.update.data = {'phone_is_confirmed': true, 'phone': props.item.phone} }"></a>
            <input class="uk-input uk-form-disabled" :class="{ 'uk-form-danger': props.errors.indexOf('phone') >= 0 }"
                   id="ch-contact-phone" v-model="props.item.phone">
        </div>
    </div>
    <div class="uk-width-2-3">
        <label class="uk-form-label" for="ch-contact-address_name">Indirizzo</label>
        <div class="uk-form-controls">
            <input class="uk-input" :class="{ 'uk-form-danger': props.errors.indexOf('address_name') >= 0 }"
                   id="ch-contact-address_name" v-model="props.item.address_name">
        </div>
    </div>
    <div class="uk-width-1-6">
        <label class="uk-form-label" for="ch-contact-address_number">Civico</label>
        <div class="uk-form-controls">
            <input class="uk-input" :class="{ 'uk-form-danger': props.errors.indexOf('address_number') >= 0 }"
                   id="ch-contact-address_number" v-model="props.item.address_number">
        </div>
    </div>
    <div class="uk-width-1-6">
        <label class="uk-form-label" for="ch-contact-zip_code">CAP</label>
        <div class="uk-form-controls">
            <input class="uk-input" :class="{ 'uk-form-danger': props.errors.indexOf('zip_code') >= 0 }"
                   id="ch-contact-zip_code" v-model="props.item.zip_code">
        </div>
    </div>
    <div class="uk-width-1-1 uk-width-1-3@m">
        <label class="uk-form-label">Comune</label>
        <ch-select
                class="uk-form-controls"
                label="denominazione_corrente"
                v-model="props.item.city_id"
                url="/api/data/comuni"
            >
        </ch-select>
    </div>
    <div class="uk-width-1-1 uk-width-1-3@m">
        <label class="uk-form-label" for="ch-contact-birthdate">Data di nascita</label>
        <div class="uk-form-controls">
            <input class="uk-input" :class="{ 'uk-form-danger': props.errors.indexOf('birthdate') >= 0 }"
                   id="ch-contact-birthdate" v-model="props.item.birthdate" v-mask="{mask: '99/99/9999', placeholder: 'GG/MM/AAAA'}" placeholder="GG/MM/AAAA">
        </div>
    </div>
    <div class="uk-width-1-1 uk-width-1-3@m" v-if="props.isNew">
        <div class="uk-form-label" for="ch-contact-privacy">Informativa Privacy</div>
        <div class="uk-form-controls">
            <label><input class="uk-checkbox" :class="{ 'uk-form-danger': props.errors.indexOf('privacy') >= 0 }" type="checkbox" v-model="props.item.privacy"/> Accettata</label>
        </div>
    </div>
    <div class="uk-width-1-1">
        <h3 class="uk-heading-divider uk-margin-top">Note</h3>
    </div>
    <div class="uk-width-1-1">
        <label class="uk-form-label" for="ch-contact-source">Provenienza</label>
        <input class="uk-input" type="text" name="ch-contact-source" v-model="props.item.source">
    </div>
    <div class="uk-width-1-1">
        <label class="uk-form-label" for="ch-contact-representative">Referente</label>
        <input class="uk-input" type="text" name="ch-contact-representative" v-model="props.item.representative">
    </div>
    <div class="uk-width-1-1">
        <label class="uk-form-label" for="ch-contact-notes">Note</label>
        <textarea class="uk-textarea" name="ch-contact-notes" v-model="props.item.notes"></textarea>
    </div>
</div>