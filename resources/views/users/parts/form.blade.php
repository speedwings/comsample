<div slot-scope="props" v-if="props.item" class="uk-grid uk-grid-small" uk-grid>
    <div class="uk-width-1-1 uk-width-1-2@m">
        <label class="uk-form-label" for="ch-user-name">Nome</label>
        <div class="uk-form-controls">
            <input class="uk-input" :class="{ 'uk-form-danger': props.errors.indexOf('name') >= 0 }" id="ch-user-name" v-model="props.item.name">
        </div>
    </div>
    <div class="uk-width-1-1 uk-width-1-2@m">
        <label class="uk-form-label" for="ch-user-email">Email</label>
        <div class="uk-form-controls">
            <input class="uk-input" :class="{ 'uk-form-danger': props.errors.indexOf('email') >= 0 }" id="ch-user-email" v-model="props.item.email">
        </div>
    </div>
    <div class="uk-width-1-1 uk-width-1-2@m">
        <label class="uk-form-label" for="ch-user-password" v-text="(props.item.id) ? 'Nuova Password' : 'Password'">Password</label>
        <div class="uk-form-controls">
            <input type="password" class="uk-input" :class="{ 'uk-form-danger': props.errors.indexOf('password') >= 0 }" id="ch-user-password" v-model="props.item.password">
        </div>
    </div>
    <div class="uk-width-1-1 uk-width-1-2@m">
        <label class="uk-form-label" for="ch-user-password_confirmation" v-text="(props.item.id) ? 'Conferma Nuova Password' : 'Conferma Password'">Conferma Password</label>
        <div class="uk-form-controls">
            <input type="password" class="uk-input" :class="{ 'uk-form-danger': props.errors.indexOf('password') >= 0 }" id="ch-user-password_confirmation" v-model="props.item.password_confirmation">
        </div>
    </div>
</div>
