@extends('layouts.app')

@section('content')
    <ch-form
            url="{{ route('api.users.show', $id) }}"
            close-uri="{{ route('users.index') }}"
            :success-modal-buttons="{
      'ok_label': 'CONTINUA',
      'cancel_label': 'CHIUDI',
      'cancel_url': '{{ route('users.index') }}'
    }"
    >
        @include('users.parts.form')
    </ch-form>
@endsection
