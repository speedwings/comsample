@extends('layouts.app')

@section('content')
    <a href="{{ route('users.create') }}" class="uk-button uk-button-primary uk-margin-bottom">Nuovo Utente</a>
    <ch-table
            url="{{ route('api.users.index') }}"
            :fields="[
        {
          name: 'name',
          sortable: true,
          label: 'Nome'
        },
        {
          name: 'email',
          sortable: true,
          label: 'Email'
        }
      ]"
    >
    </ch-table>
@endsection
