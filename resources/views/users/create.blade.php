@extends('layouts.app')

@section('content')
    <ch-form
            url="{{ route('api.users.store') }}"
            close-uri="{{ route('users.index') }}"
            :is-new="true"
            :deletable="false"
            :success-modal-buttons="{
      'ok_label': 'CONTINUA',
      'cancel_label': 'CHIUDI',
      'ok_url': '{{ route('users.create') }}',
      'cancel_url': '{{ route('users.index') }}'
    }"
    >
        @include('users.parts.form')
    </ch-form>
@endsection
