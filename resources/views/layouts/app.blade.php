<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        <div id="ch-main-navbar" class="uk-box-shadow-medium uk-navbar-container uk-navbar-primary" uk-navbar="mode: click">
            <div class="uk-container uk-container-expand uk-width-1-1">
                <nav class="uk-navbar">
                    <div class="uk-navbar-left">
                        <!-- Branding Image -->
                        <a class="uk-navbar-item uk-logo" href="{{ url('/') }}">
                            {{ config('app.name', 'Laravel') }}
                        </a>
                        @auth
                        <ul class="uk-navbar-nav uk-visible@m">
                            <li class="{{(Route::currentRouteName() === 'communications.create_type' && Route::current()->parameter('type') === 'mail') ? 'uk-active' : ''}}">
                                <a href="{{ route('communications.create_type', ['type' => 'mail']) }}">
                                    <span class="uk-icon uk-margin-small-right" uk-icon="icon: mail"></span>
                                    Nuova Email
                                </a>
                            </li>
                            <li class="{{(Route::currentRouteName() === 'communications.create_type' && Route::current()->parameter('type') === 'sms') ? 'uk-active' : ''}}">
                                <a href="{{ route('communications.create_type', ['type' => 'sms']) }}">
                                    <span class="uk-icon uk-margin-small-right" uk-icon="icon: commenting"></span>
                                    Nuovo SMS
                                </a>
                            </li>
                            <li class="{{(Route::currentRouteName() === 'communications.index') ? 'uk-active' : ''}}">
                                <a href="{{ route('communications.index') }}">
                                    <span class="uk-icon uk-margin-small-right" uk-icon="icon: list"></span>
                                    Comunicazioni
                                </a>
                            </li>
                            <li class="{{(Route::currentRouteName() === 'contacts.index') ? 'uk-active' : ''}}">
                                <a href="{{ route('contacts.index') }}">
                                    <span class="uk-icon uk-margin-small-right" uk-icon="icon: users"></span>
                                    Contatti
                                </a>
                            </li>
                            @permission('edit_users')
                            <li class="{{(Route::currentRouteName() === 'users.index') ? 'uk-active' : ''}}">
                                <a href="{{ route('users.index') }}">
                                    <span class="uk-icon uk-margin-small-right" uk-icon="icon: user"></span>
                                    Utenti
                                </a>
                            </li>
                            @endpermission
                        </ul>
                        <a class="uk-navbar-toggle uk-hidden@m" uk-navbar-toggle-icon href="#offcanvas-nav" uk-toggle></a>
                        @endauth
                    </div>

                    <div class="uk-navbar-right">
                        <ul class="uk-navbar-nav">
                            @if (Auth::guest())
                                <li><a href="{{ route('login') }}">Accedi</a></li>
                            @else
                                <li>
                                    <a href="#">{{ Auth::user()->name }}</a>
                                    <div class="uk-navbar-dropdown">
                                        <ul class="uk-nav uk-navbar-dropdown-nav">
                                            <li>
                                                <a href="{{ route('logout') }}"
                                                    onclick="event.preventDefault();
                                                             document.getElementById('logout-form').submit();">
                                                    Esci
                                                </a>

                                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                    {{ csrf_field() }}
                                                </form>
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                            @endif
                        </ul>
                    </div>

                </nav>

            </div>
        </div>
        <div class="uk-container uk-margin-large-top uk-margin-large-bottom">
            @yield('content')
        </div>
        @auth
        <div id="offcanvas-nav" uk-offcanvas>
            <div class="uk-offcanvas-bar">
                <ul class="uk-nav uk-nav-default">
                    <li class="{{(Route::currentRouteName() === 'communications.create_type' && Route::current()->parameter('type') === 'mail') ? 'uk-active' : ''}}">
                        <a href="{{ route('communications.create_type', ['type' => 'mail']) }}">
                            <span class="uk-icon uk-margin-small-right" uk-icon="icon: mail"></span>
                            Nuova Email
                        </a>
                    </li>
                    <li class="{{(Route::currentRouteName() === 'communications.create_type' && Route::current()->parameter('type') === 'sms') ? 'uk-active' : ''}}">
                        <a href="{{ route('communications.create_type', ['type' => 'sms']) }}">
                            <span class="uk-icon uk-margin-small-right" uk-icon="icon: commenting"></span>
                            Nuovo SMS
                        </a>
                    </li>
                    <li class="{{(Route::currentRouteName() === 'communications.index') ? 'uk-active' : ''}}">
                        <a href="{{ route('communications.index') }}">
                            <span class="uk-icon uk-margin-small-right" uk-icon="icon: list"></span>
                            Comunicazioni
                        </a>
                    </li>
                    <li class="{{(Route::currentRouteName() === 'contacts.index') ? 'uk-active' : ''}}">
                        <a href="{{ route('contacts.index') }}">
                            <span class="uk-icon uk-margin-small-right" uk-icon="icon: users"></span>
                            Contatti
                        </a>
                    </li>
                    @permission('edit_users')
                    <li class="{{(Route::currentRouteName() === 'users.index') ? 'uk-active' : ''}}">
                        <a href="{{ route('users.index') }}">
                            <span class="uk-icon uk-margin-small-right" uk-icon="icon: user"></span>
                            Utenti
                        </a>
                    </li>
                    @endpermission
                </ul>
            </div>
        </div>
        @endauth
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
