<html lang="{{ app()->getLocale() }}">
<head>
  <meta charset="utf-8">
  <title>Matteo Renzi - {{ $communication->subject }}</title>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
</head>

<body style="padding: 0px; margin: 0px">
  <center style="padding: 0; margin: 0px">
    <table style="width: 600px" border="0" cellspacing="0" cellpadding="0">
      <tbody>
        <tr>
          <td style="width: 50px"></td>
          <td style="width: 250px"></td>
          <td style="width: 250px"></td>
          <td style="width: 50px"></td>
        </tr>
        <tr>
          <td colspan="4" style="border-left:1px solid #777;border-right:1px solid #777;">
            <a style="color:#666;" target="_blank" href="http://www.matteorenzi.it">
              <img src="{{ \Storage::disk('public')->url('assets/dot-mail-header.jpg') }}" alt="Matteo Renzi Newsletter"
              />
            </a>
          </td>
        </tr>
        <tr>
          <td style="width: 50px;border-left:1px solid #777;border-left:1px solid #777;">&nbsp;</td>
          <td colspan="2" style="width: 506px;" align="right">
            <h2 style="text-align: right; font-family: Arial, Helvetica, sans-serif; font-size: 16px; line-height: 1.3; padding: 0px; margin: 0px; color: #333333; text-transform: capitalize;">
              <br />{{ $date->formatLocalized('%A %d %B %Y') }}</h2>
            <h1 style="text-align: right; font-family: Arial, Helvetica, sans-serif; font-size: 35px; line-height: 1.3; padding: 0px; margin: 0px; color: #333333;">{{ $communication->subject }}</h1>
          </td>
          <td style="width: 50px;border-right:1px solid #777;">&nbsp;</td>
        </tr>
        <tr>
          <td style="width: 50px;border-left:1px solid #777;">&nbsp;</td>
          <td colspan="2" style="width: 506px; padding-top: 20px;font-family: Arial, Helvetica, sans-serif;font-size:13px;" align="right">
          </td>
          <td style="width: 50px;border-right:1px solid #777;">&nbsp;</td>
        </tr>
        <tr>
          <td style="width: 50px;border-left:1px solid #777;">&nbsp;</td>
          <!-- STILE LINK style="color:#C51E30;" target="_blank"  -->
          <td colspan="2" style="font-size:14px;width: 506px; font-family: Arial, Helvetica, sans-serif; line-height: 1.4; text-align: justify;">
            <!-- FOTO -->
            <br />
            <p>
              @if (!empty($communication->header))
              <img src="{{ Storage::disk('public')->url($communication->header) }}" style="width:100%;border:0;"
                />
              @endif
            </p>
            {!! $communication->body !!}
          </td>
          <td style="width: 50px;border-right:1px solid #777;">&nbsp;</td>
        </tr>
        <tr>
          <td style="width: 50px;border-left:1px solid #777;">&nbsp;</td>
          <td colspan="2" style="width: 506px; padding-top: 20px;" align="right">
          </td>
          <td style="width: 50px;border-right:1px solid #777;">&nbsp;</td>
        </tr>
        <tr>
          <td colspan="4" style="border-left:1px solid #777;border-right:1px solid #777;" height="30">&nbsp;</td>
        </tr>
        <tr>
          <td style="width: 50px;border-left:1px solid #777;background-color:#777;">&nbsp;</td>
          <td colspan="2" style="border-left:1px solid #777;border-right:1px solid #777;background-color:#777;color:#fff; font-family: Arial, Helvetica,sans-serif;font-size:12px;text-align:justify;height:100px;valign:middle;">
            Se non vuoi pi&ugrave; ricevere le newsletter
            <a target="_blank" style="font-family: Arial, Helvetica, sans-serif;color:#fff;text-decoration:underline;"
              href="{{ $unsubscribe_url }}">clicca qui</a> per cancellarti.
            <br /> Se la procedura di disiscrizione non funziona, manda un'email vuota con oggetto "CANCELLAZIONE ENEWS" all'indirizzo
            <a style="color:#fff" href="">info@dotmedia.it</a>.
          </td>
          <td style="width: 50px;border-right:1px solid #777;background-color:#777;">&nbsp;</td>
        </tr>
      </tbody>
    </table>
  </center>
</body>

</html>