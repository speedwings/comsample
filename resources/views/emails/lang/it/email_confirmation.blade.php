# Conferma l'iscrizione alla newsletter
Ciao,
di recente ti sei iscritto alla nostra newsletter. Conferma la tua richiesta per ricevere le nostre comunicazioni.

Per attivare, clicca su Conferma. Se credi ci sia stato un errore, ignora semplicemente questo messaggio.
