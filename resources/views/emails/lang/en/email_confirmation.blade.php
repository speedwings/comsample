# Confirm newsletter subscription
Hi,
recently you subscribed to our newsletter. Confirm the subscription to receive our mails.

To confirm, click on the following button. Otherwise, simply ignore this message.
