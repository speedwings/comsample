@component('mail::layout')
{{-- Header --}}
@slot('header')
@component('mail::header', ['url' => config('app.url')])
    <img src="{{ $logo  }}" alt="{{ config('app.name') }}" />
@endcomponent
@endslot

{{-- Body --}}
@include("emails.lang.$requestLocale.email_confirmation")

@component('mail::button', ['url' => $url, 'color' => 'red'])
    {{ trans('newsletter.confirm') }}
@endcomponent

{{ trans('newsletter.thanks') }},<br>
{{ config('app.name') }}

{{-- Subcopy --}}
@isset($subcopy)
@slot('subcopy')
@component('mail::subcopy')
{{ $subcopy }}
@endcomponent
@endslot
@endisset

{{-- Footer --}}
@slot('footer')
@component('mail::footer')
&copy; {{ date('Y') }} {{ config('app.name') }}. {{ trans('newsletter.all_rights_reserved') }}.
@endcomponent
@endslot
@endcomponent
