<?php

return [
	'insufficient_credits' => 'Insufficient Credits',
	'update_error' => 'Server error while updating',
	'send_error' => 'Errore while sending the communication.',
];