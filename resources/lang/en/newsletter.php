<?php

    return [
        'store_success' => 'Contact succesfully saved',
        'update_success' => 'Data successfully saved',
        'wrong_or_empty_token' => 'Invalid link',
        'invalid_type' => 'Invalid contact type',
        'already_confirmed' => 'Newsletter subscription already confirmed',
        'confirm_success' => 'Newsletter subscription successfully completed',
        'server_error' => 'Internal server error',
        'subscription_success' => 'Successfully subscribed',
        'unsubscription_success' => 'Successfully unsubscribed',
        'contact_not_found' => 'The requested contact was not found',
        'confirmation_subject' => 'Newsletter subscription confirmation',
        'confirmation_subcopy' => 'Please confirm your subscription to receive our newsletter.',
        'confirm' => 'Confirm',
        'thanks' => 'Thanks',
        'all_rights_reserved' => 'All rights reserved',
    ];
