<?php

    return [
        'store_success' => 'Contatto salvato con successo',
        'update_success' => 'Dati salvati con successo',
        'wrong_or_empty_token' => 'Link non valido',
        'invalid_type' => 'Tipo di contatto non valido',
        'already_confirmed' => 'Hai già confermato l\'iscrizione alla newsletter',
        'confirm_success' => 'Hai completato con successo l\'iscrizione alla newsletter',
        'server_error' => 'Errore del server',
        'subscription_success' => 'Iscrizione avvenuta con successo',
        'unsubscription_success' => 'Cancellazione avvenuta con successo',
        'contact_not_found' => 'Il contatto richiesto non è stato trovato',
        'confirmation_subject' => 'Conferma l\'iscrizione alla newsletter',
        'confirmation_subcopy' => "La presente conferma è indispensabile per verificare che la registrazione sia stata effettivamente richiesta dal titolare dell'indirizzo email.\n\nL'iscrizione non sarà confermata senza aver fatto click sul bottone riportato sopra.",
        'confirm' => 'Conferma',
        'thanks' => 'Grazie',
        'all_rights_reserved' => 'Tutti i diritti riservati',
    ];
