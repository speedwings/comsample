<?php

return [
	'insufficient_credits' => 'Credito insufficiente',
	'update_error' => 'Errore del server durante l\'aggiornamento.',
	'send_error' => 'Errore di invio. Contattare l\'assistenza.',
];