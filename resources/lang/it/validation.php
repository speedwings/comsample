<?php

return [

    'invalid' => 'I dati forniti non sono validi.',

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => 'Il campo :attribute dev\'essere accettato.',
    'active_url'           => 'The :attribute is not a valid URL.',
    'after'                => 'The :attribute must be a date after :date.',
    'after_or_equal'       => 'The :attribute must be a date after or equal to :date.',
    'alpha'                => 'The :attribute may only contain letters.',
    'alpha_dash'           => 'The :attribute may only contain letters, numbers, and dashes.',
    'alpha_num'            => 'The :attribute may only contain letters and numbers.',
    'array'                => 'The :attribute must be an array.',
    'before'               => 'The :attribute must be a date before :date.',
    'before_or_equal'      => 'The :attribute must be a date before or equal to :date.',
    'between'              => [
	    'numeric' => 'Il campo :attribute dev\'essere compreso tra :min e :max.',
        'file'    => 'The :attribute must be between :min and :max kilobytes.',
	    'string'  => 'Il campo :attribute dev\'essere compreso tra :min e :max caratteri.',
	    'array'   => 'Il campo :attribute dev\'essere compreso tra :min e :max elementi.',
    ],
    'boolean'              => 'The :attribute field must be true or false.',
    'confirmed'            => 'La conferma :attribute non corrisponde.',
    'date'                 => ':Attribute non è una data valida.',
    'date_format'          => 'Il formato del campo :attribute non è valido.',
    'different'            => 'The :attribute and :other must be different.',
    'digits'               => 'The :attribute must be :digits digits.',
    'digits_between'       => 'The :attribute must be between :min and :max digits.',
    'dimensions'           => 'The :attribute has invalid image dimensions.',
    'distinct'             => 'The :attribute field has a duplicate value.',
    'email'                => ':Attribute non valido.',
    'exists'               => 'The selected :attribute is invalid.',
    'file'                 => 'The :attribute must be a file.',
    'filled'               => 'The :attribute field must have a value.',
    'image'                => 'The :attribute must be an image.',
    'in'                   => 'The selected :attribute is invalid.',
    'in_array'             => 'The :attribute field does not exist in :other.',
    'integer'              => 'The :attribute must be an integer.',
    'ip'                   => 'The :attribute must be a valid IP address.',
    'ipv4'                 => 'The :attribute must be a valid IPv4 address.',
    'ipv6'                 => 'The :attribute must be a valid IPv6 address.',
    'json'                 => 'The :attribute must be a valid JSON string.',
    'max'                  => [
	    'numeric' => 'Il campo :attribute al massimo :max.',
        'file'    => 'The :attribute may not be greater than :max kilobytes.',
	    'string'  => 'Il campo :attribute dev\'essere al massimo di :max caratteri.',
	    'array'   => 'Il campo :attribute dev\'essere al massimo di :max elementi.',
    ],
    'mimes'                => 'The :attribute must be a file of type: :values.',
    'mimetypes'            => 'The :attribute must be a file of type: :values.',
    'min'                  => [
	    'numeric' => 'Il campo :attribute almeno :min.',
        'file'    => 'The :attribute must be at least :min kilobytes.',
	    'string'  => 'Il campo :attribute almeno di :min caratteri.',
	    'array'   => 'Il campo :attribute almeno di :min elementi.',
    ],
    'not_in'               => 'The selected :attribute is invalid.',
    'numeric'              => 'Il campo :attribute dev\'essere un numero.',
    'present'              => 'The :attribute field must be present.',
    'regex'                => 'Il campo :attribute ha un formato non valido.',
    'required'             => ':Attribute obbligatorio.',
    'required_if'          => 'Il campo :attribute è obbligatorio quando :other è :value.',
    'required_unless'      => 'The :attribute field is required unless :other is in :values.',
    'required_with'        => 'Il campo :attribute è obbligatorio se è presente :values.',
    'required_with_all'    => 'The :attribute field is required when :values is present.',
    'required_without'     => 'Indicare almeno uno tra :attribute e :values.',
    'required_without_all' => 'The :attribute field is required when none of :values are present.',
    'same'                 => 'The :attribute and :other must match.',
    'size'                 => [
        'numeric' => 'The :attribute must be :size.',
        'file'    => 'The :attribute must be :size kilobytes.',
        'string'  => 'The :attribute must be :size characters.',
        'array'   => 'The :attribute must contain :size items.',
    ],
    'string'               => 'The :attribute must be a string.',
    'timezone'             => 'The :attribute must be a valid zone.',
    'unique'               => ':Attribute già presente.',
    'uploaded'             => 'The :attribute failed to upload.',
    'url'                  => 'The :attribute format is invalid.',
    // Custom rules
    'date_multiformat'     => ':Attribute non è una data valida.',
    'unique_confirmed'     => ':Attribute già presente.',
    'unique_confirmed_or_unsubscribed'     => 'Risulti già iscritto alla newsletter.',
    'phone'                => ':Attribute non è un numero valido',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'email' => [
            'unique' => ':Attribute già presente tra gli iscritti.',
            'required_if' => 'L\':attribute è necessario per attivare l\'iscrizione.'
        ],
	    'phone' => [
	    	'required_if' => 'Il :attribute è necessario per attivare l\'iscrizione.'
	    ],
	    'title' => [
	    	'required_without' => 'Indicare il titolo o l\'oggetto, se presente.'
        ],
        'privacy' => [
            'accepted' => 'Devi accettare l\'informativa sulla privacy.',
            'required' => 'Il campo Privacy dev\'essere confermato.'
        ]
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [
    	'name' => 'nome',
        'email' => 'indirizzo email',
        'firstname' => 'nome',
        'lastname' => 'cognome',
        'gender' => 'sesso',
        'phone' => 'telefono',
        'address_name' => 'indirizzo',
	    'address_number' => 'numero civico',
        'birthdate' => 'data di nascita',
        'email_confirmed_at' => 'email confermata il',
        'email_subscribed_at' => 'email iscritta il',
        'email_unsubscribed_at' => 'email cancellata il',
        'phone_confirmed_at' => 'telefono confermato il',
        'phone_subscribed_at' => 'telefono iscritto il',
        'phone_unsubscribed_at' => 'telefono cancellato il',
        'email_is_subscribed' => 'iscrizione newsletter',
        'phone_is_subscribed' => 'iscrizione sms',
	    'notes' => 'note',
	    'source' => 'provenienza',
	    'representative' => 'referente',
	    'title' => 'titolo',
	    'subject' => 'oggetto',
	    'body' => 'contenuto',
    ],

];
