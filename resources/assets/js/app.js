/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
const VueInputMask = require('vue-inputmask').default
Vue.use(VueInputMask);
Vue.use(require('vue-shortkey'));
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// Components
Vue.component('ch-table', require('./components/ChTable.vue'));
Vue.component('ch-form', require('./components/ChForm.vue'));
Vue.component('ch-loading-spinner', require('./components/ChLoadingSpinner.vue'));
Vue.component('ch-editor', require('./components/ChEditor.vue'));
Vue.component('ch-sms-textarea', require('./components/ChSmsTextarea.vue'));
Vue.component('ch-uploader', require('./components/ChUploader.vue'));
Vue.component('ch-mail-preview', require('./components/ChMailPreview.vue'));
Vue.component('ch-api-button', require('./components/ChApiButton.vue'));
Vue.component('ch-autocomplete', require('./components/ChAutocomplete.vue'));
Vue.component('ch-select', require('./components/ChSelect'));
Vue.component('ch-api-label', require('./components/ChApiLabel'));
Vue.component('ch-segments-manager', require('./components/ChSegmentsManager'));

const app = new Vue({
  el: '#app',
  mounted() {
    UIkit.sticky('#ch-main-navbar', {
      offset: -10
    })
  }
});
