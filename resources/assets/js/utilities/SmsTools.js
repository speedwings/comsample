export default {

  maxLengthForEncoding: {
    gsm: {
      normal: 160,
      concatenated: 153
    },
    unicode: {
      normal: 70,
      concatenated: 67
    }
  },

  doubleByteChars: {
    '^': true,
    '{': true,
    '}': true,
    '[': true,
    '~': true,
    ']': true,
    '|': true,
    '€': true,
    '\\': true
  },

  gsmEncodingPattern: /^[0-9a-zA-Z@Δ¡¿£_!Φ"¥Γ#èΛ¤éΩ%ùΠ&ìΨòΣçΘΞ:Ø;ÄäøÆ,<Ööæ=ÑñÅß>ÜüåÉ§à€~ \$\.\-\+\(\)\*\\\/\?\|\^\}\{\[\]\'\r\n]*$/,

  doubleBytePattern: /[~€\^\}\{\]\[\|\\]/,

  sanitizePattern: /[^0-9a-zA-Z@Δ¡¿£_!Φ"¥Γ#èΛ¤éΩ%ùΠ&ìΨòΣçΘΞ:Ø;ÄäøÆ,<Ööæ=ÑñÅß>ÜüåÉ§à€~ \$\.\-\+\(\)\*\\\/\?\|\^\}\{\[\]\'\r\n]+/g,

  counter: {},

  getCounter(text) {
    this.counter.text = text.replace(/\r\n?/g, "\n")
    this.counter.length = this.getLength()
    this.counter.parts = this.getPartsCount()
    this.counter.maxLength = (this.counter.parts === 1) ? this.maxLengthForEncoding[this.counter.encoding].normal : this.maxLengthForEncoding[this.counter.encoding].concatenated * this.counter.parts
    console.log(this.counter)
    return this.counter
  },

  getLength() {
    if (this.counter.encoding == 'gsm') {
      let length = 0
      this.counter.trimmed = ''
      for (let char of Array.from(this.counter.text)) {
        if (this.doubleByteChars[char]) {
          length += 2
        } else {
          length++
        }
        if (length <= this.maxLengthForEncoding['gsm'].normal) {
          this.counter.trimmed += char
        }
      }
      return length
    } else {
      return this.counter.text.length
    }
  },

  getPartsCount() {
    if (this.counter.length <= this.maxLengthForEncoding[this.counter.encoding].normal) {
      return 1
    } else {
      return parseInt(Math.ceil(this.counter.length / this.maxLengthForEncoding[this.counter.encoding].concatenated), 10)
    }
  }

}