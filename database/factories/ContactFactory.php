<?php

use Faker\Generator as Faker;

$factory->define(ComHub\Contact::class, function (Faker $faker) {

    $lastname = $faker->optional($weight = 0.8)->lastName;
    $firstname = ($lastname) ? $faker->optional($weight = 0.8)->firstName : null;

    $email = $faker->optional($weight = 0.7)->safeEmail;
    $phone = $faker->optional($weight = 0.3)->phoneNumber;
    if (!empty($phone)) $phone = (string) phone($phone, 'IT');

    $email_confirmed = ($email) ? $faker->optional($weight = 0.8)->dateTimeThisYear($max = 'now', $timezone = 'Europe/Rome') : null;
    $phone_confirmed = ($phone) ? $faker->optional($weight = 0.8)->dateTimeThisYear($max = 'now', $timezone = 'Europe/Rome') : null;

    $email_subscribed = ($email_confirmed) ? $faker->boolean(50) : false;
    $phone_subscribed = ($phone_confirmed) ? $faker->boolean(50) : false;

    $address = $faker->optional($weight = 0.4)->streetName;

	$cityCount = \ComHub\Services\ComuneIstat::count() - 1;
	$offset = $faker->optional($weight = 0.8)->numberBetween(0, $cityCount);
	$cityId = ($cityCount > 0 && $offset != null) ? \ComHub\Services\ComuneIstat::offset($offset)->first()->id : null;

    return [
        'firstname' => $firstname,
        'lastname' => $lastname,
        'gender' => $faker->randomElement([null, 'M', 'F']),
        'birthdate' => ($date = $faker->optional(0.4)->dateTimeBetween('-70 years', '-18 years')) ? $date->format('d/m/Y') : null,
        'email' => $email,
        'email_confirmed_at' => $email_confirmed,
        'phone_confirmed_at' => $phone_confirmed,
        'email_subscribed_at' => ($email_subscribed && $faker->boolean(70)) ? $faker->dateTimeThisYear($max = 'now', $timezone = 'Europe/Rome') : null,
        'email_unsubscribed_at' => ($email_subscribed && $faker->boolean(40)) ? $faker->dateTimeThisYear($max = 'now', $timezone = 'Europe/Rome') : null,
        'phone' => $phone,
        'phone_subscribed_at' => ($phone_subscribed && $faker->boolean(70)) ? $faker->dateTimeThisYear($max = 'now', $timezone = 'Europe/Rome')  : null,
        'phone_unsubscribed_at' => ($phone_subscribed &&  $faker->boolean(40)) ? $faker->dateTimeThisYear($max = 'now', $timezone = 'Europe/Rome')  : null,
        'address_name' => $address,
	    'address_number' => ($address) ? $faker->optional($weight = 0.9)->buildingNumber : null,
	    'zip_code' => $faker->optional($weight = 0.4)->postcode,
	    'notes' => $faker->optional($weight = 0.2)->paragraph,
	    'source' => $faker->randomElement([null, 'website', 'form', 'facebook']),
	    'representative' => $faker->name,
        'created_at' => $faker->dateTimeBetween('-60 days', 'now'),
        'updated_at' => $faker->dateTimeBetween('-60 days', 'now'),
	    'city_id' => $cityId,
    ];
});
