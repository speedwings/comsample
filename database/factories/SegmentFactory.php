<?php

use Faker\Generator as Faker;

$factory->define(ComHub\Segment::class, function (Faker $faker) {

	$userIds = ComHub\User::pluck('id')->toArray();

	$created = $faker->boolean;

	return [
		'user_id' => $faker->randomElement($userIds),
		'description' => ($created) ? $faker->sentence : null,
		'filters' => [],
		'created' => $created
	];
});