<?php

use Faker\Generator as Faker;

$factory->define(\ComHub\Communication::class, function(Faker $faker) {

	$userIds = \ComHub\User::pluck('id')->toArray();

	return [
//		'type_id' => ,//);
		'user_id' => $faker->randomElement($userIds),//);
//		'segment_id' => ,//)->nullable();

		'subject' => $faker->sentence(4, true),//)->nullable();
//		'header' => ,//)->nullable();
		'body' => $faker->text(140),//)->nullable();

//		'sent_request_at' => ,//)->nullable();
//		'sent_done_at' => ,//)->nullable();
//		'sent_error' => ,//)->nullable();
//
//		'recipients_count' => ,//)->nullable();
	];

});
