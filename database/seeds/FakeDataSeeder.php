<?php

use Illuminate\Database\Seeder;
use ComHub\Contact;

class FakeDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    $faker = \Faker\Factory::create('it_IT');
	    ComHub\User::get()->each(function($user) use ($faker) {
		    \Auth::login($user);
		    factory(Contact::class, $faker->numberBetween(0, 50))->create();
	    });
    }
}
