<?php

use ComHub\Permission;
use ComHub\Role;
use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    $admin = Role::create(['name' => 'admin', 'description' => 'Administrator']);
	    $admin->permissions()->attach(Permission::get());
    }
}
