<?php

use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$now = \Carbon\Carbon::now();
		\DB::table('permissions')->insert([
			[
				'name' => 'edit_users',
				'description' => 'Gestione Utenti',
				'created_at' => $now,
				'updated_at' => $now,
			],
			[
				'name' => 'view_all_contacts',
				'description' => 'Visualizza tutti i Contatti',
				'created_at' => $now,
				'updated_at' => $now,
			]
		]);
    }
}
