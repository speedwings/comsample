<?php

use Illuminate\Database\Seeder;

class CommunicationTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = \Carbon\Carbon::now();
        \DB::table('communication_types')->insert([
        	[
        		'name' => 'SMS',
		        'route' => 'phone',
		        'price' => 0,
		        'template' => null,
		        'created_at' => $now,
		        'updated_at' => $now,
	        ],
	        [
		        'name' => 'Mail',
		        'route' => 'email',
		        'price' => 0,
		        'template' => 'default',
		        'created_at' => $now,
		        'updated_at' => $now,
	        ],
        ]);
    }
}
