<?php

use Illuminate\Database\Seeder;
use ComHub\User;
use ComHub\Role;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    $admin = Role::where('name', 'admin')->first();

	    User::create([
            'name' => env('ADMIN_NAME', 'admin'),
            'email' => env('ADMIN_EMAIL', 'aspedali@dotmedia.it'),
            'password' => bcrypt(env('ADMIN_PASSWORD', 'password')),
            'remember_token' => str_random(10)
        ])->roles()->attach($admin);

	    (User::create([
		    'name' => 'Davide',
		    'email' => 'd.bacarella@gmail.com',
		    'password' => bcrypt('d07m3d1a'),
		    'remember_token' => str_random(10)
	    ]))->roles()->attach($admin);

    }
}
