<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contacts', function (Blueprint $table) {
            $table->increments('id');
	        $table->unsignedInteger('user_id')->nullable();
	        $table->unsignedInteger('city_id')->nullable();

	        $table->string('firstname')->nullable();
            $table->string('lastname')->nullable();
            $table->enum('gender', ['M', 'F'])->nullable();
            $table->date('birthdate')->nullable();

            $table->string('email')->nullable();
            $table->timestamp('email_confirmed_at')->nullable();
            $table->timestamp('email_subscribed_at')->nullable();
            $table->timestamp('email_unsubscribed_at')->nullable();
            $table->string('email_token')->nullable();

            $table->string('phone')->nullable();
            $table->timestamp('phone_confirmed_at')->nullable();
            $table->timestamp('phone_subscribed_at')->nullable();
            $table->timestamp('phone_unsubscribed_at')->nullable();
            $table->string('phone_token')->nullable();

            $table->string('address_name')->nullable();
            $table->string('address_number')->nullable();

	        $table->string('zip_code', 5)->nullable();

            $table->text('notes')->nullable();
            $table->string('source')->nullable();
	        $table->string('representative')->nullable();

	        $table->integer('score')->default(0);

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contacts');
    }
}
