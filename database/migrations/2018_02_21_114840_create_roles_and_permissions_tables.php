<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRolesAndPermissionsTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::create('roles', function(Blueprint $table) {
		    $table->increments('id');
		    $table->string('name')->unique();
		    $table->string('description');
		    $table->softDeletes();
		    $table->timestamps();
	    });

	    Schema::create('permissions', function(Blueprint $table) {
		    $table->increments('id');
		    $table->string('name')->unique();
		    $table->string('description');
		    $table->softDeletes();
		    $table->timestamps();
	    });

	    Schema::create('role_user', function(Blueprint $table) {

		    $table->integer('role_id')->unsigned();
		    $table->integer('user_id')->unsigned();

		    $table->primary(['role_id', 'user_id']);
		    $table->index(['role_id', 'user_id']);
	    });

	    Schema::create('permission_user', function(Blueprint $table) {
		    $table->integer('permission_id')->unsigned();
		    $table->integer('user_id')->unsigned();

		    $table->primary(['permission_id', 'user_id']);
		    $table->index(['permission_id', 'user_id']);
	    });

	    Schema::create('permission_role', function(Blueprint $table) {
		    $table->integer('permission_id')->unsigned();
		    $table->integer('role_id')->unsigned();

		    $table->primary(['permission_id', 'role_id']);
		    $table->index(['permission_id', 'role_id']);
	    });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::dropIfExists('roles');
	    Schema::dropIfExists('permissions');
	    Schema::dropIfExists('role_user');
	    Schema::dropIfExists('permission_user');
	    Schema::dropIfExists('permission_role');
    }
}
