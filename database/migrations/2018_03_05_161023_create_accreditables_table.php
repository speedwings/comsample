<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccreditablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('accreditables', function (Blueprint $table) {
            $table->unsignedInteger('credit_id');
            $table->unsignedInteger('accreditable_id');
            $table->string('accreditable_type');

            $table->index(['credit_id', 'accreditable_id', 'accreditable_type'], 'accreditable_id')->unique();

            $table->foreign('credit_id')->references('id')->on('credits')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accreditables');
    }
}
