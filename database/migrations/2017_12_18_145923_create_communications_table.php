<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommunicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('communications', function (Blueprint $table) {
            $table->increments('id');
	        $table->unsignedInteger('type_id');
	        $table->unsignedInteger('user_id');

	        $table->unsignedInteger('segment_id')->nullable();
            $table->text('filters')->nullable();

            $table->string('subject')->nullable();
            $table->string('header')->nullable();
            $table->text('body')->nullable();

            $table->timestamp('sent_request_at')->nullable();
            $table->timestamp('sent_done_at')->nullable();
            $table->text('sent_error')->nullable();

            $table->unsignedInteger('recipients_count')->nullable();

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('communications');
    }
}
