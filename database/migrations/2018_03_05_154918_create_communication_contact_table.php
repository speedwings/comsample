<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommunicationContactTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('communication_contact', function (Blueprint $table) {
            $table->unsignedInteger('communication_id');
            $table->unsignedInteger('contact_id');
            $table->longText('details')->nullable();

	        $table->index(['communication_id', 'contact_id'], 'communication_contact_id')->unique();

	        $table->foreign('communication_id')->references('id')->on('communications')->onDelete('cascade');
	        $table->foreign('contact_id')->references('id')->on('contacts')->onDelete('cascade');

	        $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('communication_contact');
    }
}
