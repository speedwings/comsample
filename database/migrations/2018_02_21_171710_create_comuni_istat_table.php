<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComuniIstatTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comuni_istat', function (Blueprint $table) {
            $table->increments('id');

            $table->string('codice_regione', 2);
            $table->string('codice_citta_metropolitana', 3)->nullable();
            $table->string('codice_provincia', 3);
            $table->string('progressivo_comune', 3);
            $table->string('codice_comune_alfanumerico', 6);
            $table->string('denominazione_corrente');
	        $table->string('denominazione_altra_lingua')->nullable();
	        $table->unsignedTinyInteger('codice_ripartizione_geografica');
	        $table->string('ripartizione_geografica');
	        $table->string('denominazione_regione');
	        $table->string('denominazione_citta_metropolitana')->nullable();
	        $table->string('denominazione_provincia')->nullable();
	        $table->boolean('flag_capoluogo')->default(0);
	        $table->string('sigla_automobilistica', 2);
	        $table->unsignedInteger('codice_comune_numerico');
	        $table->unsignedInteger('codice_comune_numerico_10_16');
	        $table->unsignedInteger('codice_comune_numerico_06_09');
	        $table->unsignedInteger('codice_comune_numerico_95_05');
	        $table->string('codice_catastale', 4);
	        $table->unsignedInteger('popolazione_legale');
	        $table->string('codice_nuts1_10', 3)->nullable();
	        $table->string('codice_nuts2_10', 4)->nullable();
	        $table->string('codice_nuts3_10', 5)->nullable();
	        $table->string('codice_nuts1_06', 3)->nullable();
	        $table->string('codice_nuts2_06', 4)->nullable();
	        $table->string('codice_nuts3_06', 5)->nullable();

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comuni_istat');
    }
}
