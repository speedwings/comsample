<?php

return [
	'frontendUri' => env( 'FRONTEND_URI', 'http://localhost' ),
	'confirmUri'  => env( 'CONFIRM_URI', '/' ),
	'unsubscribeUri' => env('UNSUBSCRIBE_URI', '/'),
	'istatPermalink' => env('ISTAT_PERMALINK'),
];