<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
        'options' => [
            'endpoint' => env('SPARKPOST_ENDPOINT')
        ]
    ],

    'stripe' => [
        'model' => ComHub\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],

    'skebby' => [
        'debug' => env('SKEBBY_DEBUG', false),
        'debug_request' => env('SKEBBY_DEBUG_REQUEST', false),
        'pretend' => env('SKEBBY_PRETEND', true),
        'gateway' => env('SKEBBY_GATEWAY'),
        'username' => env('SKEBBY_USERNAME'),
        'password' => env('SKEBBY_PASSWORD'),
        'sender' => env('SKEBBY_SENDER'),
    ]

];
