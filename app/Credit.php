<?php

namespace ComHub;

use ComHub\Traits\Filtrable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Credit extends Model
{
    use SoftDeletes, Filtrable;

    protected $available;

    protected $fillable = [
        'description',
	    'amount',
	    'notes',
    ];

    protected $dates = [
    	'deleted_at',
    ];

    public function getAmountAttribute()
    {
    	return $this->attributes['amount'] / 100;
    }

    public function setAmountAttribute($val)
    {
    	$this->attributes['amount'] = $val * 100;
    }

    public function communications()
    {
    	return $this->morphedByMany(Communication::class, 'accreditable')->withTimestamps();
    }
}
