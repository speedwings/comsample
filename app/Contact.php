<?php

namespace ComHub;

use Carbon\Carbon;
use ComHub\Services\ComuneIstat;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Str;
use ComHub\Traits\Filtrable;
use Validator;

class Contact extends Model
{
    use SoftDeletes, Filtrable, Notifiable;

    protected $fillable = [
        'firstname',
        'lastname',
        'gender',
        'birthdate',
        'email',
        'email_confirmed_at',
        'email_subscribed_at',
        'email_unsubscribed_at',
        'phone',
        'phone_confirmed_at',
        'phone_subscribed_at',
        'phone_unsubscribed_at',
        'address_name',
	    'address_number',
        'zip_code',
	    'notes',
	    'source',
        'representative',
        'privacy_accepted_at',

	    // Relationships
	    'city_id',
	    'user_id',

        // Virtual
        'email_is_subscribed',
        'phone_is_subscribed',
        'email_is_confirmed',
        'phone_is_confirmed',
        'privacy'
    ];

    protected $dates = [
        'birthdate',
        'email_confirmed_at',
        'phone_confirmed_at',
        'email_subscribed_at',
        'email_unsubscribed_at',
        'phone_subscribed_at',
        'phone_unsubscribed_at',
        'deleted_at',
        'privacy_accepted_at',
    ];

    protected $appends = [
        'email_is_subscribed',
        'phone_is_subscribed',
        'privacy',
    ];

	public function scopeOwned($query, $userId = null)
	{
		if (\Auth::user()->hasPermission('view_all_contacts')) return $query;
		$userId = ($userId == null) ? \Auth::user()->id : $userId;
		return $query->where('user_id', $userId);
	}


	public function scopeEmailable($query) {
        return $query->whereNotNull('email')->whereNotNull('email_confirmed_at');
    }

    public function scopeTextable($query) {
        return $query->whereNotNull('phone')->whereNotNull('phone_confirmed_at');
    }

    public function scopeNotifiable($query, $field)
    {
        return $query->where( function($query) use ($field) {
            return $query->whereNotNull($field)
                         ->whereNotNull($field.'_confirmed_at')
                         ->whereNotNull($field.'_subscribed_at')
                         ->whereNull($field.'_unsubscribed_at');
        });
    }

    public function generateToken() {
        return Str::random(60);
    }

    public function getEmailIsSubscribedAttribute() {
	    if (empty($this->attributes['email_subscribed_at'])) return null;
	    return !empty($this->attributes['email_subscribed_at']) && empty($this->attributes['email_unsubscribed_at']);
    }

    public function setEmailIsSubscribedAttribute($val) {
	    if ($val === null) return;
	    if ($val) {
            if (empty($this->attributes['email_unsubscribed_at'])) {
                $this->attributes['email_subscribed_at'] = Carbon::now();
            } else {
                $this->attributes['email_unsubscribed_at'] = null;
            }
        } else {
            $this->attributes['email_unsubscribed_at'] = Carbon::now();
        }
    }

    public function getPhoneIsSubscribedAttribute() {
	    if (empty($this->attributes['phone_subscribed_at'])) return null;
	    return !empty($this->attributes['phone_subscribed_at']) && empty($this->attributes['phone_unsubscribed_at']);
    }

    public function setPhoneIsSubscribedAttribute($val) {
	    if ($val === null) return;
	    if ($val) {
            $this->attributes['phone_subscribed_at'] = Carbon::now();
            if (!empty($this->attributes['phone_unsubscribed_at'])) {
                $this->attributes['phone_unsubscribed_at'] = null;
            }
        } else {
            $this->attributes['phone_unsubscribed_at'] = Carbon::now();
        }
    }

    public function setEmailIsConfirmedAttribute($val) {
        if ($val && empty($this->attributes['email_confirmed_at']) && !empty($this->attributes['email'])) {
            $this->attributes['email_confirmed_at'] = Carbon::now();
        }
    }

    public function setPhoneIsConfirmedAttribute($val) {
        if ($val && empty($this->attributes['phone_confirmed_at']) && !empty($this->attributes['phone'])) {
            $this->attributes['phone_confirmed_at'] = Carbon::now();
        }
    }

	public function getBirthdateAttribute() {
		if ($this->attributes['birthdate'])
			return (new Carbon($this->attributes['birthdate']))->format('d/m/Y');
		return null;
	}

	public function setBirthdateAttribute($val) {
		try {
			$birthdate = Carbon::createFromFormat('d/m/Y', $val);
		} catch(\InvalidArgumentException $e) {
			$birthdate = null;
		}
		$this->attributes['birthdate'] = $birthdate;
    }

    public function setPrivacyAttribute($val) {
        if (empty($this->attributes['privacy_accepted_at'])) {
            $this->attributes['privacy_accepted_at'] = Carbon::now();
        }
    }

    public function getPrivacyAttribute() {
        return !empty($this->attributes['privacy_accepted_at']);
    }

	public function routeNotificationForSkebby() {
      if (
        !Validator::make(
            [
                'phone' => $this->phone
            ],
            [
                'phone' => ['required', 'phone:IT']
            ]
        )->fails()
      ) {
          return $this->phone;
      }
      return null;
    }

	public function user()
	{
		return $this->belongsTo(User::class);
	}

	public function city()
	{
		return $this->belongsTo(ComuneIstat::class);
	}

	public function communications()
	{
		return $this->belongsToMany(Communication::class)->withTimestamps();
	}

}
