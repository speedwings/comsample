<?php

namespace ComHub\Listeners;

use ComHub\Events\ContactReceivedCommunication;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class ContactScorePoints implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ContactReceivedCommunication  $event
     * @return void
     */
    public function handle(ContactReceivedCommunication $event)
    {
        $event->contact->score += $event->points;
        $event->contact->save();
    }
}
