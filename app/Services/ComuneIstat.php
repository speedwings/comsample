<?php

namespace ComHub\Services;

use ComHub\Contact;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ComuneIstat extends Model
{
	use SoftDeletes;

    protected $table = 'comuni_istat';

    public function contacts()
    {
    	return $this->hasMany(Contact::class);
    }

}
