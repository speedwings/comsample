<?php

    namespace ComHub\Jobs;

    use Carbon\Carbon;
    use ComHub\Communication;
    use ComHub\Contact;
    use ComHub\Events\CommunicationSent;
    use ComHub\Events\ContactReceivedCommunication;
    use ComHub\Http\Controllers\Api\SettingsRestController;
    use ComHub\Notifications\Clients\SkebbyClient;
    use ComHub\Notifications\Exception\SkebbyException;
    use ComHub\Notifications\Messages\SkebbyMessage;
    use Illuminate\Bus\Queueable;
    use Illuminate\Queue\SerializesModels;
    use Illuminate\Queue\InteractsWithQueue;
    use Illuminate\Contracts\Queue\ShouldQueue;
    use Illuminate\Foundation\Bus\Dispatchable;

    class MassSmsNotification implements ShouldQueue
    {
        use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

        protected $communication;
        protected $offset;
        protected $count;

        /**
         * Create a new job instance.
         *
         * @return void
         */
        public function __construct(Communication $communication)
        {
            $this->communication = $communication;
        }

        /**
         * Execute the job.
         *
         * @return void
         */
        public function handle(SkebbyClient $skebby)
        {
            $message = new SkebbyMessage($this->communication->body, $this->communication->reference);
            $recipients = $this->communication->contacts;

            try {
                $skebby->send(
                    $message,
                    $recipients->pluck('phone')->toArray());
            } catch (\Exception $e) {
                $this->communication->sent_request_at = null;
                $this->communication->sent_error = $skebby->getLastMessage();
	            $this->communication->save();
                throw new SkebbyException();
            }

            if ($skebby->getLastStatusCode() > 299) {
                $this->communication->sent_request_at = null;
                $this->communication->sent_error = $skebby->getLastMessage();
	            $this->communication->save();
                throw new SkebbyException();
            } else if ($skebby->getLastStatusCode() >= 200) {
                $count = count($recipients);
                $this->communication->sent_done_at = Carbon::now();
                $this->communication->recipients_count = $count;
                event(new CommunicationSent($this->communication, $count));
                $recipients->each(function($contact) use ($recipients) {
                    event(new ContactReceivedCommunication($contact));
                });
            }
            $this->communication->save();
        }
    }
