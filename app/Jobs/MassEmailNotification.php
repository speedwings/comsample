<?php

namespace ComHub\Jobs;

use Carbon\Carbon;
use ComHub\Communication;
use ComHub\Events\CommunicationSent;
use ComHub\Events\ContactReceivedCommunication;
use ComHub\Notifications\Clients\SparkPostClient;
use ComHub\Notifications\Exception\SparkPostException;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Transport\SparkPostTransport;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class MassEmailNotification implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $communication;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Communication $communication)
    {
        $this->communication = $communication;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(SparkPostClient $sparkpost)
    {
        $recipients = $this->communication->contacts;
        $template = 'mail-templates.' . ($this->communication->type->template ?? 'default');
        $body = view($template)->with([
	        'communication' => $this->communication,
	        'date' => $communication->sent_at ?? Carbon::now(),
	        'unsubscribe_url' => ltrim( config( 'comhub.frontendUri' ) . config( 'comhub.unsubscribeUri' ), '/' ) . '/{{unsubscribe_token}}'
        ])->render();
//        $recipientsAddress = $this->recipientsRoutes($recipients, $this->communication->type->route);
        $unique_id = $this->communication->getKey() . ' - ' . $this->communication->title;
		$message = (new \Swift_Message())
		->setSubject($this->communication->subject)
		->setFrom([config('mail.from.address') => config('mail.from.name')])
		->setContentType('text/html')
		->setBody($body);
//		->setTo($recipientsAddress);
		try {
			$sparkpost->setUniqueId($unique_id);
			$sparkpost->setRecipients($recipients);
			$sparkpost->send($message);
		} catch (\Exception $e) {
			$this->communication->sent_request_at = null;
			$this->communication->sent_error = trans('comhub.send_error');
			$this->communication->save();
			throw new SparkPostException($e->getMessage());
		}
		$count = $sparkpost->getRecipientsCount();
	    $this->communication->sent_error = null;
	    $this->communication->sent_done_at = Carbon::now();
	    $this->communication->recipients_count = $count;
	    event(new CommunicationSent($this->communication, $count));
	    $recipients->each(function($contact) use ($recipients) {
		    event(new ContactReceivedCommunication($contact));
	    });
		$this->communication->save();
    }

    protected function recipientsRoutes($recipients, $field) {
    	if (is_string($recipients)) return [$recipients];
    	$routes = [];
    	foreach ($recipients as $recipient) {
    		$routes[$recipient->$field] = $recipient->firstname . ' ' . $recipient->lastname;
	    }
	    return $routes;
    }
}
