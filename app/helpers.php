<?php

    if (!function_exists('localize_url')) {
        function localize_url($url, $locale, $addDefault = false) {
            $localeTag = '%locale%';
            if (!is_numeric(strpos($url, $localeTag))) return $url;
            $urlParts = explode('/', $url);
            $isDefault = $locale === config('app.defaultLocale');
            $urlParts = array_filter(array_map(function($part) use ($addDefault, $isDefault, $localeTag, $locale) {
                if ($part === $localeTag) {
                    return ($isDefault && !$addDefault) ? null : $locale;
                }
                return $part;
            }, $urlParts));
            return implode('/', $urlParts);
        }
    }
