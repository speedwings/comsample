<?php

namespace ComHub\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class EmailConfirmation extends Mailable
{
    use Queueable, SerializesModels;

    public $email;
    public $url;
    public $logo;
    public $requestLocale;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($email, $url, $locale)
    {
        $this->email = $email;
        $this->url = $url;
        $this->requestLocale = $locale;
        $this->logo = \Storage::disk('public')->url('assets/logo.png');
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        app()->setLocale($this->requestLocale);
        return $this->subject(trans('newsletter.confirmation_subject'))->markdown('emails.email_confirmation');
    }
}
