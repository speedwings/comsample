<?php

	namespace ComHub\Observers;

	use ComHub\Segment;

	class SegmentObserver {
		public function creating(Segment $segment)
		{
			$segment->user_id = ($segment->user_id) ?? \Auth::user()->id;
		}
	}