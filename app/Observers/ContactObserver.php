<?php

namespace ComHub\Observers;

use ComHub\Contact;
use Carbon\Carbon;

class ContactObserver {
	public function saving( Contact $contact ) {
		if ( empty( $contact->email_token ) && ! empty( $contact->email ) ) {
			$contact->email_token = $contact->generateToken();
		}
		if ( ! empty( $contact->phone ) ) {
			if ( empty( $contact->phone_token ) ) {
				$contact->phone_token = $contact->generateToken();
			}
			if ( empty( $contact->phone_confirmed_at ) ) {
				$contact->phone_confirmed_at = Carbon::now();
			}
		}
	}

	public function creating( Contact $contact ) {
		$contact->user()->associate( \Auth::user() );
	}

}