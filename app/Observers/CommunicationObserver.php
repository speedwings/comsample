<?php


namespace ComHub\Observers;


use ComHub\Communication;

class CommunicationObserver {
	public function creating(Communication $communication) {
		if (empty($communication->type_id)) {
			$communication->type_id = 1;
		}
		$communication->user_id = ($communication->user_id) ?? \Auth::user()->id;
	}

	public function saving(Communication $communication) {
		if (empty($communication->title)) $communication->title = $communication->subject;
	}
}