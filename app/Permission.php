<?php

namespace ComHub;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Permission extends Model
{
	use SoftDeletes;

	protected $fillable = [
		'name'
	];

	public function users()
	{
		return $this->belongsToMany(User::class);
	}

	public function roles()
	{
		return $this->belongsToMany(Role::class);
	}
}
