<?php

namespace ComHub;

use ComHub\Traits\Filtrable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Segment extends Model
{
    use SoftDeletes, Filtrable;

    protected $fillable = [
    	'user_id',
	    'description',
	    'filters'
    ];

    protected $dates = [
    	'deleted_at'
    ];

    protected $casts = [
    	'filters' => 'json',
	    'created' => 'bool'
    ];

    public function communications() {
    	return $this->hasMany(Communication::class);
    }
}
