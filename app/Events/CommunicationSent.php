<?php

namespace ComHub\Events;

use ComHub\Communication;
use ComHub\Contact;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class CommunicationSent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

	public $communication;
    public $recipientsCount;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Communication $communication, int $recipientsCount)
    {
    	$this->communication = $communication;
        $this->recipientsCount = $recipientsCount;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
