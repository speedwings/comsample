<?php


namespace ComHub\Traits;


use ComHub\Permission;
use ComHub\Role;

trait HasRolesAndPermissions {

	protected $roleClass = Role::class;
	protected $permissionClass = Permission::class;

	public function roles()
	{
		return $this->belongsToMany($this->roleClass);
	}

	public function permissions()
	{
		return $this->belongsToMany($this->permissionClass);
	}

	public function hasRole($role)
	{
		$role = (is_string($role)) ? $role : $role->name;
		return $this->roles()->where('name', $role)->count() > 0;
	}

	public function hasPermission($permission)
	{
		$permission = (is_string($permission)) ? $permission : $permission->name;
		return (
			       $this->permissions()->where('name', $permission)->count() +
			       $this->roles()->whereHas('permissions', function($query) use ($permission) {
				       $query->where('name', $permission);
			       })->count()
		       ) > 0;
	}
}
