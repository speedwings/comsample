<?php

namespace ComHub\Traits;


use ComHub\Filters\BaseFilters;

trait Filtrable
{

	protected $searchFields = ['name'];

    public function scopeFilter($query, BaseFilters $filters)
    {
      return $filters->apply($query);
    }

}