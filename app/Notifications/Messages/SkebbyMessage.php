<?php


namespace ComHub\Notifications\Messages;


class SkebbyMessage
{
  public $content;
  public $reference;

  public function __construct($content = '', $reference = null)
  {
    $this->content = $content;
    $this->reference = $reference;
  }

  public function content($content)
  {
    $this->content = $content;
  }

	public function reference($reference)
	{
		$this->reference = $reference;
	}
}