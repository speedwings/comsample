<?php


namespace ComHub\Notifications\Exception;

use \Exception;
use Throwable;

class SparkPostException extends Exception
{
	public function report()
	{

	}

	public function render($request)
	{
		return response($this->getMessage());
	}
}