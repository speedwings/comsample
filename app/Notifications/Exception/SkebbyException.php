<?php


namespace ComHub\Notifications\Exception;

use \Exception;

class SkebbyException extends Exception
{
    public function report()
    {
        
    }

    public function render($request)
    {
        return response('Errore di Skebby');
    }
}