<?php


namespace ComHub\Notifications\Channels;


use ComHub\Notifications\Clients\SkebbyClient;
use ComHub\Notifications\Exception\SkebbyException;
use ComHub\Notifications\Messages\SkebbyMessage;
use Illuminate\Notifications\Notification;

class SkebbySmsChannel
{

    private $skebby;

    public function __construct(SkebbyClient $client)
    {
        $this->skebby = $client;
    }

    public function send($notifiable, Notification $notification)
    {
        if (! $to = $notifiable->routeNotificationFor('skebby')) {
            return;
        }
        $message = $notification->toSkebby($notifiable);

        if (is_string($message)) {
            $message = new SkebbyMessage($message);
        }

        $this->skebby->send($message, $to);

        if ($this->skebby->getLastStatusCode() != 200) {
            throw new SkebbyException($this->skebby->getLastMessage(), $this->skebby->getLastStatusCode());
        }
    }
}