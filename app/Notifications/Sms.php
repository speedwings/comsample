<?php


namespace ComHub\Notifications;


use ComHub\Communication;
use ComHub\Notifications\Channels\SkebbySmsChannel;
use ComHub\Notifications\Messages\SkebbyMessage;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Notification;

class Sms extends Notification implements ShouldQueue
{
  use Queueable;

  private $communication;

  public function __construct(Communication $communication)
  {
    $this->communication = $communication;
  }

  public function via($notifiable)
  {
    return [SkebbySmsChannel::class];
  }

  public function toSkebby($notifiable)
  {
    return new SkebbyMessage($this->communication->body);
  }

}