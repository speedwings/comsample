<?php


namespace ComHub\Notifications\Clients;

use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\MessageFormatter;
use GuzzleHttp\Middleware;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Mail\Transport\SparkPostTransport;
use GuzzleHttp\Client as HttpClient;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;

class SparkPostClient extends SparkPostTransport {

	protected $sent;
	protected $count;
	protected $uniqueId = '';
	/**
	 * @var \Illuminate\Database\Eloquent\Collection $recipients
	 */
	protected $recipients = null;

	protected $logger;
	private $requestLogFormat = "{method} {uri} HTTP/{version} {req_body}";
	private $responseLogFormat = "RESPONSE: {res_headers}, {code} - {res_body}";

	public function __construct($options = []) {
		$config = config('services.sparkpost');
		parent::__construct(
			new HttpClient(Arr::add(
				$config['guzzle'] ?? [], 'connect_timeout', 60
			)),
			$config['secret'],
			array_merge($config['options'] ?? [], $options)
		);
		$this->sent = false;
		$this->count = 0;

		$this->logger = Log::channel('sparkpost');

		$stack = HandlerStack::create();
		$stack->push( $this->createLogMiddleware($this->responseLogFormat) );
		$stack->push( $this->createLogMiddleware($this->requestLogFormat) );

		$this->client = new Client(['handler' => $stack]);
	}

	private function createLogMiddleware(string $messageFormat) {
		return Middleware::log(
			$this->logger,
			new MessageFormatter($messageFormat)
		);
	}

	public function setUniqueId($uniqueId) {
		$this->uniqueId = ($uniqueId) ?? '';
	}

	public function setRecipients($recipients) {
		$this->recipients = ($recipients) ?? [];
	}

	public function send(\Swift_Mime_SimpleMessage $message, &$failedRecipients = null) {
		$this->beforeSendPerformed($message);

		$recipients = $this->getRecipients($message);

		$message->setBcc([]);

		$response = $this->client->post('https://api.sparkpost.com/api/v1/transmissions', [
			'headers' => [
				'Authorization' => $this->key,
			],
			'json' => array_merge([
				'recipients' => $recipients,
				'campaign_id' => $this->uniqueId,
				'content' => [
					'from' => config('mail.from.address'),
					'subject' => $message->getSubject(),
					'html' => $message->getBody(),
				],
			]),
		]);
		$message->getHeaders()->addTextHeader(
			'X-SparkPost-Transmission-ID', $this->getTransmissionId($response)
		);

		$this->sendPerformed($message);

		$this->sent = true;
		$this->count = $this->numberOfRecipients($message);

		return $this->count;
	}

	public function getRecipientsCount() {
		if ($this->sent) return $this->count;
		return 0;
	}

	protected function numberOfRecipients(\Swift_Mime_SimpleMessage $message)
	{
		if ($this->recipients) return $this->recipients->count();
		return count(array_merge(
			(array) $message->getTo(), (array) $message->getCc(), (array) $message->getBcc()
		));
	}

	protected function getRecipients(\Swift_Mime_SimpleMessage $message) {
		if ($this->recipients) return $this->getCollectionRecipients();
		if ($message) return $this->getMessageRecipients($message);
		return [];
	}

	protected function getMessageRecipients(\Swift_Mime_SimpleMessage $message)
	{
		$recipients = [];

		foreach ((array) $message->getTo() as $email => $name) {
			$recipients[] = [
				'address' => compact('name', 'email')
			];
		}

		foreach ((array) $message->getCc() as $email => $name) {
			$recipients[] = [
				'address' => compact('name', 'email')
			];
		}

		foreach ((array) $message->getBcc() as $email => $name) {
			$recipients[] = [
				'address' => compact('name', 'email')
			];
		}

		return $recipients;
	}

	protected function getCollectionRecipients() {
		$recipients = [];
		$this->recipients->each(function($contact) use (&$recipients) {
			$recipients[] = [
				'address' => [
					'name' => $contact->first_name . ' ' . $contact->last_name,
					'email' => $contact->email
				],
				'substitution_data' => [
					'firstname' => $contact->first_name,
					'lastname' => $contact->last_name,
					'unsubscribe_token' => $contact->email_token
				]
			];
		});
		return $recipients;
	}

}