<?php


    namespace ComHub\Notifications\Clients;


    use ComHub\Notifications\Exception\SkebbyException;
    use ComHub\Notifications\Messages\SkebbyMessage;
    use GuzzleHttp\Client;
    use GuzzleHttp\Exception\RequestException;
    use GuzzleHttp\HandlerStack;
    use GuzzleHttp\MessageFormatter;
    use GuzzleHttp\Middleware;
    use GuzzleHttp\Psr7\Request;
    use GuzzleHttp\Psr7\Response;
    use Illuminate\Support\Facades\Log;

    class SkebbyClient
    {
        private $gateway;
        private $username;
        private $password;
        private $userKey;
        private $sessionKey;
        private $sender;
        private $debug;
        private $debugRequest;
        private $pretend;
        private $client;
        private $requestLogFormat = "{method} {uri} HTTP/{version} {req_body}";
        private $responseLogFormat = "RESPONSE: {code} - {res_body}";

        const CLASSIC_PLUS = 'GP';
        const CLASSIC = 'TI';
        const BASIC = 'SI';
        const ESTERO = 'EE';

        /**
         * Custom Logger
         *
         * @var \Monolog\Logger;
         */
        private $logger;

        private $statusCode = null;
        private $message = null;

        public function __construct(
            $gateway,
            $username,
            $password,
            $sender,
            $debug,
            $debugRequest,
            $pretend
        )
        {
            $this->gateway = $gateway;
            $this->username = $username;
            $this->password = $password;
            $this->sender = $sender;
            $this->debug = $debug;
            $this->debugRequest = $debugRequest;
            $this->pretend = $pretend;

            $this->logger = Log::channel('skebby');

            $stack = HandlerStack::create();
            if ($this->debug) $stack->push( $this->createLogMiddleware($this->responseLogFormat) );
            if ($this->debugRequest) $stack->push( $this->createLogMiddleware($this->requestLogFormat) );

            $this->client = new Client(['base_uri' => $this->gateway, 'handler' => $stack]);

            $this->retrieveKeys();
        }

        private function createLogMiddleware(string $messageFormat) {
            return Middleware::log(
                $this->logger,
                new MessageFormatter($messageFormat)
            );
        }

        public function send(SkebbyMessage $message, $recipients)
        {
            $recipients = (is_array($recipients)) ? $recipients : [$recipients];
            $this->logAttempt(count($recipients), $message->content);
            if ($this->pretend) {
            	$this->fakeSend();
            } else {
	            try {
		            $response = $this->client->request('POST', 'sms', [
			            'headers' => $this->getHeader(),
			            'debug' => $this->debugRequest,
			            'body' => $this->getBody($message, $recipients)
		            ]);
		            $this->setResponse($response);
	            } catch (RequestException $e) {
		            if ($e->hasResponse()) {
			            $this->setResponse($e->getResponse());
		            } else {
			            $this->statusCode = 400;
			            $this->message = 'Bad Request';
		            }
	            }
            }
        }

        private function getUserKey()
        {
            if (!$this->userKey) $this->retrieveKeys();
            return $this->userKey;
        }

        private function getSessionKey()
        {
            if (!$this->sessionKey) $this->retrieveKeys();
            return $this->sessionKey;
        }

        private function retrieveKeys()
        {
            try {
                $response = $this->client->request('GET', 'login', [
                    'headers' => [
                        'Content-Type' => 'application/json',
                    ],
                    'debug' => $this->debugRequest,
                    'query' => [
                        'username' => $this->username,
                        'password' => $this->password,
                    ]
                ]);

                $credentials = explode(';', (string)$response->getBody());
                $this->userKey = $credentials[0];
                $this->sessionKey = $credentials[1];
            } catch (RequestException $e) {

            }

        }

        private function getHeader()
        {
            return [
                'Content-Type' => 'application/json',
                'user_key' => $this->getUserKey(),
                'Session_key' => $this->getSessionKey(),
            ];
        }

        private function getBody(SkebbyMessage $message,$recipients)
        {
            return \GuzzleHttp\json_encode([
                'order_id' => $message->reference,
                'recipient' => $recipients,
                'message_type' => SkebbyClient::CLASSIC,
                'message' => $message->content,
                'sender' => $this->sender
            ]);
        }

        private function setResponse(Response $response)
        {
            $this->statusCode = $response->getStatusCode();
            if ((int)$this->statusCode > 299) $this->message = trans('comhub.send_error');
//            $this->message = $this->formatMessage((string) $response->getBody());
        }

        private function formatMessage($message)
        {
            libxml_use_internal_errors(true);
            if ($xml = simplexml_load_string($message)) {
                $code = $xml->code ?? $this->statusCode;
                $text = $xml->error_message ?? 'REST Error';
                return "$code - $text";
            } else if ($json = \GuzzleHttp\json_decode($message)) {
                $code = $json->code ?? $this->statusCode;
                $text = $json->error_message ?? 'REST Error';
                return "$code - $text";
            } else {
                return $message;
            }
        }

        private function logAttempt($count, $message)
        {
            $this->logger->addInfo("Sending attempt to $count contacts with message: \"$message\"");
        }

        private function fakeSend()
        {
        	$this->statusCode = 200;
        	$this->message = 'OK';
        }

        public function getLastStatusCode()
        {
            return ($this->statusCode) ?: 400;
        }

        public function getLastMessage()
        {
            return ($this->message) ?: 'Undefined Error';
        }
    }