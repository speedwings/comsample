<?php

namespace ComHub;

use ComHub\Traits\Filtrable;
use Illuminate\Database\Eloquent\Model;

class CommunicationType extends Model
{
	use Filtrable;

    protected $fillable = [
    	'name',
	    'route',
	    'price',
	    'quantity'
    ];

	public function getPriceAttribute()
	{
		return $this->attributes['price'] / 100;
	}

	public function setPriceAttribute($val)
	{
		$this->attributes['price'] = $val * 100;
	}

	public function communications()
	{
		return $this->hasMany(Communication::class);
	}
}
