<?php

namespace ComHub;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Setting extends Model
{
	use SoftDeletes;

    protected $primaryKey = 'key';

    protected $fillable = [
    	'values'
    ];

	protected $dates = [
		'deleted_at'
	];

    protected $casts = [
    	'key' => 'string',
    	'values' => 'json',
	    'rules' => 'json'
    ];

    public function getValue($val)
    {
    	return json_decode($this->attributes['values'], true)[$val];
    }

}
