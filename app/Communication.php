<?php

namespace ComHub;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use ComHub\Traits\Filtrable;

class Communication extends Model
{

    const READY = 'ready';
    const PENDING = 'pending';
    const SENT = 'sent';

    use SoftDeletes, Filtrable;

    protected $fillable = [
    	'title',
        'subject',
        'header',
        'body',
	    'type_id',
	    'segment_id',
	    'filters',
    ];

    protected $dates = [
        'deleted_at',
        'sent_request_at',
        'sent_done_at',
    ];

    protected $casts = [
    	'filters' => 'json',
	    'sent_error' => 'json'
    ];

    protected $appends = [ 'sent', 'state' ];

    public function getSentAttribute()
    {
        return !empty($this->attributes['sent_done_at']);
    }

    public function getStateAttribute()
    {
        if (!empty($this->attributes['sent_request_at'])) {
            $state = (empty($this->attributes['sent_done_at'])) ? self::PENDING : self::SENT;
        } else {
            $state = self::READY;
        }
        return $state;
    }

    public function getBatches()
    {
        return ceil($this->contacts()->count() / $this->type->quantity);
    }

    public function getCost()
    {
        return $this->type->price * $this->getBatches();
    }

    public function getCreditLabel()
    {
        return $this->attributes['subject'] ?? 'Comunication '.$this->getKey();
    }

    // TODO: create trait ExternalReferenceable with defeault methods and necessary properties
    public function getReference()
    {
    	return str_slug(config('app.name')) . '_' . $this->getKey();
    }

    public function countRecipients()
    {
	    return Contact::notifiable($this->type->route)->get()->filter(
		    function ($contact) {
			    return !!$contact->routeNotificationForSkebby();
		    }
	    )->count();
    }

    public function type()
    {
    	return $this->belongsTo(CommunicationType::class);
    }

    public function segment()
    {
    	return $this->belongsTo(Segment::class);
    }

    public function user()
    {
    	return $this->belongsTo(User::class);
    }

    public function contacts()
    {
    	return $this->belongsToMany(Contact::class)->withTimestamps();
    }

    public function credits()
    {
    	return $this->morphToMany(Credit::class, 'accreditable')->withTimestamps();
    }
}
