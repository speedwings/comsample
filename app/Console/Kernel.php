<?php

namespace ComHub\Console;

use ComHub\Console\Commands\UpdateIstat;
use ComHub\Console\Commands\ImportContacts;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        UpdateIstat::class,
        ImportContacts::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();
        if ($crontabUser = env('CRONTAB_USER', null)) {
            $crontabUser = (strlen($crontabUser) > 8) ? substr($crontabUser, 0, 7).'+' : $crontabUser;
            if (stripos((string) shell_exec("ps aux | grep '[q]ueue:work' | grep '$crontabUser'"), 'artisan queue:work') === false) {
                $schedule->command('queue:work --queue=high,default,low --sleep=5 --tries=3')->everyMinute()->appendOutputTo(storage_path() . '/logs/scheduler.log');
            }
        }
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
