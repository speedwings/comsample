<?php

namespace ComHub\Console\Commands;

use ComHub\Credit;
use Illuminate\Console\Command;

class ChangeCredits extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'credits:change';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add or remove a Credits amount.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $amount = $this->ask('Indicate the amount:');
        $description = $this->ask('Specify the reason:') ?? 'Administrative record';
        if ($credit = Credit::create([
        	'amount' => $amount,
	        'description' => $description
        ])) {
	        $this->info("New Credits record created:");
	        $this->info("\t- Amount: $amount");
	        $this->info("\t- Record ID: $credit->id");
        } else {
        	$this->error('Error creating new Credits record.');
        }
    }
}
