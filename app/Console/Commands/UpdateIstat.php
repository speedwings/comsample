<?php

namespace ComHub\Console\Commands;

use Carbon\Carbon;
use ComHub\Services\ComuneIstat;
use Illuminate\Console\Command;
use League\Csv\CharsetConverter;
use League\Csv\Reader;
use League\Csv\Statement;

class UpdateIstat extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:istat {--force}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update ISTAT data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
    	$forced = $this->option('force');
    	$dataPresent = ComuneIstat::count() > 0;
    	if ($dataPresent && !$forced) {
			$this->error('ISTAT data already present.');
		    return $this->info('Run the command with --force option to delete the available data and import new one.');
	    }
	    $table = \DB::table('comuni_istat');
	    if ($dataPresent) {
	    	if ($deleted = $table->delete()) {
			    $this->info("Table cleared. Deleted $deleted rows.");
		    } else {
	    		return $this->error("Error clearing table.");
		    };
	    }
    	$istatUrl = config('comhub.istatPermalink');
    	if (empty($istatUrl)) return $this->error('No ISTAT permalink provided.');
    	try {
		    $data = file_get_contents($istatUrl);
	    } catch(\Exception $e) {
    		$error = $e->getMessage();
    		return $this->error("Unable to download the file: $error");
	    }
	    if (\Storage::disk('imports')->put('comuni_istat.csv', $data)) {
	    	$now = Carbon::now();
	    	$csv = Reader::createFromPath(\Storage::disk('imports')->path('comuni_istat.csv'));
	    	$csv->setHeaderOffset(0);
	    	$csv->setDelimiter(';');
	    	CharsetConverter::addTo($csv, 'ISO-8859-1', 'UTF-8');
	    	$records = $csv->getRecords();
	    	$rows = iterator_count($records);
	    	$voids = 0;
	    	$errors = 0;
	    	$bar = $this->output->createProgressBar($rows);
	    	foreach ($records as $row) {
	    		if (!empty($row['Denominazione in italiano'])) {
				    $item = [
					    'codice_regione'                    => $this->formatField( $row['Codice Regione'] ),
					    'codice_citta_metropolitana'        => $this->formatField( $row['Codice Città Metropolitana'] ),
					    'codice_provincia'                  => $this->formatField( $row['Codice Provincia (1)'] ),
					    'progressivo_comune'                => $this->formatField( $row['Progressivo del Comune (2)'] ),
					    'codice_comune_alfanumerico'        => $this->formatField( $row['Codice Comune formato alfanumerico'] ),
					    'denominazione_corrente'            => $this->formatField( $row['Denominazione in italiano'] ),
					    'denominazione_altra_lingua'        => $this->formatField( $row['Denominazione altra lingua'] ),
					    'codice_ripartizione_geografica'    => $this->formatField( $row['Codice Ripartizione Geografica'] ),
					    'ripartizione_geografica'           => $this->formatField( $row['Ripartizione geografica'] ),
					    'denominazione_regione'             => $this->formatField( $row['Denominazione regione'] ),
					    'denominazione_citta_metropolitana' => $this->formatField( $row['Denominazione Città metropolitana'] ),
					    'denominazione_provincia'           => $this->formatField( $row['Denominazione provincia'] ),
					    'flag_capoluogo'                    => (bool) $this->formatField( $row['Flag Comune capoluogo di provincia'] ),
					    'sigla_automobilistica'             => $this->formatField( $row['Sigla automobilistica'] ),
					    'codice_comune_numerico'            => $this->formatField( $row['Codice Comune formato numerico'] ),
					    'codice_comune_numerico_10_16'      => $this->formatField( $row['Codice Comune numerico con 110 province (dal 2010 al 2016)'] ),
					    'codice_comune_numerico_06_09'      => $this->formatField( $row['Codice Comune numerico con 107 province (dal 2006 al 2009)'] ),
					    'codice_comune_numerico_95_05'      => $this->formatField( $row['Codice Comune numerico con 103 province (dal 1995 al 2005)'] ),
					    'codice_catastale'                  => $this->formatField( $row['Codice Catastale del comune'] ),
					    'popolazione_legale'                => $this->formatField( $row['Popolazione legale 2011 (09/10/2011)'] ),
					    'codice_nuts1_10'                   => $this->formatField( $row['Codice NUTS1 2010'] ),
					    'codice_nuts2_10'                   => $this->formatField( $row['Codice NUTS2 2010 (3) '] ),
					    'codice_nuts3_10'                   => $this->formatField( $row['Codice NUTS3 2010'] ),
					    'codice_nuts1_06'                   => $this->formatField( $row['Codice NUTS1 2006'] ),
					    'codice_nuts2_06'                   => $this->formatField( $row['Codice NUTS2 2006 (3)'] ),
					    'codice_nuts3_06'                   => $this->formatField( $row['Codice NUTS3 2006'] ),
					    'created_at'                        => $now,
					    'updated_at'                        => $now,
				    ];
				    if (!$table->insert( $item )) $errors++;
			    } else {
	    			$voids++;
			    }
			    $bar->advance();
		    }
		    $count = $table->count();
	    	$this->info("\nInserted $count rows of $rows:");
		    $this->info("\t- $voids empty.");
		    $this->info("\t- $errors errors.");
	    };
    }

    private function formatField($field = null)
    {
    	if (empty($field) || $field == '-' || $field == '') return null;
    	$undotted = str_replace('.', '', $field);
    	if (is_numeric($undotted)) return $undotted;
    	return trim($field);
    }
}
