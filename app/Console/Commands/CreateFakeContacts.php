<?php

namespace ComHub\Console\Commands;

use ComHub\Contact;
use ComHub\User;
use Illuminate\Console\Command;

class CreateFakeContacts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fake:contacts {count=1}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create fake Contacts.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
	    $faker = \Faker\Factory::create('it_IT');
	    $userIds = User::pluck('id')->toArray();
	    $bar = $this->output->createProgressBar($this->argument('count'));
	    for ($i = 0; $i < $this->argument('count'); $i++) {
		    factory(Contact::class)->create([
		    	'user_id' => $faker->randomElement($userIds)
		    ]);
		    $bar->advance();
	    }
    }
}
