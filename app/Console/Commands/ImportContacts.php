<?php

namespace ComHub\Console\Commands;

use Illuminate\Console\Command;
use ComHub\Http\Controllers\Import\ImportContactsController;

class ImportContacts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'contacts:import
                {table : Table containing data}
                {transformer : Transformer class}
                {--connection : Connection to data origin}
                {--skip= : Data offset}
                {--limit= : Data limit}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import contacts from external source';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $importer = new ImportContactsController(
            $this->argument('table'),
            $this->argument('transformer'),
            $this->option('connection') ?? null,
            $this->option('skip') ?? null,
            $this->option('limit') ?? null
        );

        $ids = $importer->listKeys();
        $bar = $this->output->createProgressBar($ids->count());
        $ids->each(function($id) use ($importer, $bar) {
            try {
                $importer->import($id);
            } catch (\Exception $e) {
                $this->error($e);
            }
            $bar->advance();
        });
    }
}
