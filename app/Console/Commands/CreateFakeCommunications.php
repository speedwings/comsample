<?php

namespace ComHub\Console\Commands;

use ComHub\Communication;
use ComHub\Contact;
use ComHub\User;
use Illuminate\Console\Command;

class CreateFakeCommunications extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fake:communications {count=1}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create fake communications.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
	    $faker = \Faker\Factory::create('it_IT');
	    $userIds = User::pluck('id')->toArray();
	    if (empty($userIds)) {
	    	return $this->error('No Users available');
	    }
	    $contactIds = Contact::pluck('id')->toArray();
	    $bar = $this->output->createProgressBar($this->argument('count'));
	    for ($i = 0; $i < $this->argument('count'); $i++) {
		    if ($communication = factory(Communication::class)->create([
			    'user_id' => $faker->randomElement($userIds)
		    ])) {
			    $communication->contacts()->attach(
				    $faker->randomElements(
					    $contactIds,
					    $faker->numberBetween( floor( count( $contactIds ) / 5 ), count( $contactIds ) )
				    )
			    );
		    }
		    $bar->advance();
	    }
    }
}
