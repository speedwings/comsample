<?php

  namespace ComHub\Filters;

  use Carbon\Carbon;

  class ContactFilters extends BaseFilters
  {
    public function order_by($val = null)
    {
      if (empty($val)) return $this->query;
      $this->query->orderBy($this->request->get('order_by'), $this->request->get('order', 'asc'));
    }

    public function search($string = null)
    {
      if (empty($string)) return $this->query;
      $search = $this->request->get('search');
      $this->query->where(function ($query) use ($search) {
        return $query->where('firstname', 'LIKE', "%$search%")
          ->orWhere('lastname', 'LIKE', "%$search%")
          ->orWhere('email', 'LIKE', "%$search%")
          ->orWhere('phone', 'LIKE', "%$search%");
      });
    }

    public function gender($gender = null)
    {
      if (empty($gender)) return $this->query;
      $this->query->where('gender', $gender);
    }

    public function source($source = null)
    {
    	if (empty($source)) return $this->query;
    	$this->query->where('source', 'like', "%$source%");
    }

    public function city($city = null)
    {
    	if (empty($city)) return $this->query;
    	$this->query->where('city_id', $city);
    }

    public function age_min($age = null)
    {
      if (empty($age) || !is_numeric($age)) return $this->query;
      $this->ageFilter('<=', $age);
    }

    public function age_max($age = null)
    {
      if (empty($age) || !is_numeric($age)) return $this->query;
      $this->ageFilter('>=', $age);
    }

    public function email_state($state = null)
    {
      if (empty($state)) return $this->query;
      return $this->contactState('email', $state);
    }

    public function phone_state($state = null)
    {
      if (empty($state)) return $this->query;
      return $this->contactState('phone', $state);
    }

    private function ageFilter($op, $age)
    {
      $this->query->whereNotNull('birthdate')->where('birthdate', $op, Carbon::now()->addYear(-$age));
    }

    private function contactState($field, $state)
    {
      switch ($state) {
          case 'empty':
              $this->query->whereNull($field);
              break;
          case 'available':
              $this->query->whereNotNull($field);
              break;
          case 'unsubscribed':
              $this->query->where(function($query) use ($field) {
                  return $query->whereNull($field.'_subscribed_at')->orWhereNotNull($field.'_unsubscribed_at');
              });
              break;
          case 'subscribed':
              $this->query->whereNotNull($field.'_confirmed_at')->whereNotNull($field.'_subscribed_at')->whereNull($field.'_unsubscribed_at');
              break;
          case 'confirmed':
              $this->query->whereNotNull($field.'_confirmed_at');
              break;
      }
    }
  }