<?php


namespace ComHub\Filters;


class SegmentFilters extends BaseFilters {

	public function search($string = null)
	{
		if (empty($string)) return $this->query;
		return $this->query->where('description', 'like', "%$string%");
	}
}