<?php
namespace ComHub\Filters;


use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class BaseFilters
{
  protected $request;
  protected $query;
  protected $description;
  protected $filters;

  /**
   * BaseFilters constructor.
   * @param $request
   */
  public function __construct(Request $request, $filters = null)
  {
    $this->request = $request;
    $this->filters = $filters;
  }

  public function apply(Builder $query)
  {
    $this->query = $query;

    foreach ($this->filters() as $name => $value) {
      if (method_exists($this, $name)) {
        call_user_func_array([$this, $name], array_filter([trim($value)]));
      }
    }

    return $this->query;
  }

  public function filters()
  {
    return ($this->filters) ?? $this->request->all();
  }

  protected function id($string = null)
  {
    if (empty($string)) return $this->query;
    return $this
      ->query
      ->where('id', $string);
  }

}