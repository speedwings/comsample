<?php


namespace ComHub\Filters;


class UserFilters extends BaseFilters
{
	public function order_by($val = null)
	{
		if (empty($val)) return $this->query;
		$this->query->orderBy($this->request->get('order_by'), $this->request->get('order', 'asc'));
	}

	public function search($string = null)
	{
		if (empty($string)) return $this->query;
		$search = $this->request->get('search');
		$this->query->where(function ($query) use ($search) {
			return $query->where('name', 'LIKE', "%$search%")
			             ->orWhere('email', 'LIKE', "%$search%");
		});
	}
}
