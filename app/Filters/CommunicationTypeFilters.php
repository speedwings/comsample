<?php


namespace ComHub\Filters;


class CommunicationTypeFilters extends BaseFilters {

	public function search($string = null)
	{
		if (empty($string)) return $this->query;
		return $this->query->where('name', 'like', "%$string%");
	}
}