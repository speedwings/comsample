<?php

namespace ComHub\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Carbon\Carbon;
use ComHub\Communication;

class MailPreviewController extends Controller
{
    public function html($id)
    {
        $communication = Communication::with(['type'])->when(Auth::guest(), function($query) {
            return $query->whereNotNull('sent_at');
        })->findOrFail($id);
        $date = ($communication->sent_at) ? $communication->sent_at : Carbon::now();
        return view('mail-templates/'.$communication->type->template)->with([
            'communication' => $communication,
            'date' => $date,
	        'unsubscribe_url' => '#'
        ]);
    }
}
