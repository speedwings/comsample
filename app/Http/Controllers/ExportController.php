<?php

namespace ComHub\Http\Controllers;

use Illuminate\Http\Request;
use ComHub\Contact;
use Excel;
use Carbon\Carbon;
use ComHub\Filters\ContactFilters;

class ExportController extends Controller
{
    public function exportContacts(Request $request, ContactFilters $filters, $format, $localized = true) {
        $contacts = Contact::filter($filters)->get();
        $filename = 'export-contacts-'.Carbon::now()->format('j-m-Y');
	    $data = $this->format($contacts->flatten()->toArray(), $localized);
        return $this->generate($filename, $format, $data);
    }

    private function generate($filename, $format, $data, $sheet = null) {
        $sheet = ($sheet) ?: $filename;
        return Excel::create($filename, function($excel) use ($data, $sheet) {
            $excel->sheet($sheet, function($sheet) use ($data) {
                $sheet->fromArray($data);
            });
        })->download($format);
    }

	private function format($array, $localize)
    {
	    if (count($array) == 0) return [];
	    $array = array_map(function($item) {
		    $fields = ['firstname', 'lastname', 'email', 'phone', 'address_name', 'address_number', 'notes', 'source', 'representative', 'email_subscribed_at', 'email_unsubscribed_at', 'phone_subscribed_at', 'phone_unsubscribed_at'];
		    return array_only($item, $fields);
	    }, $array);
	    if ($localize) {
		    $firstRow = [];
		    foreach ( $array[0] as $key => $value ) {
			    $firstRow[ trans( "validation.attributes.$key" ) ] = $value;
		    }
		    $array[0] = $firstRow;
	    }
	    return $array;
    }
}
