<?php

namespace ComHub\Http\Controllers;

use ComHub\CommunicationType;
use Illuminate\Http\Request;
use ComHub\Communication;
use ComHub\Contact;

class CommunicationsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('communications.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($type = null)
    {
    	$type_id = null;
    	if ($type) {
    		$comm = CommunicationType::where('name', $type)->first();
    		if ($comm) $type_id = $comm->id;
	    }
        return view('communications.create')->with([
        	'type_id' => $type_id
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $communication = Communication::findOrFail($id);
        $recipients_count = $communication->contacts()->count();
        return view('communications.show')->with([
            'communication' =>$communication,
            'recipients_count' => $recipients_count
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('communications.edit')->with(['id' => $id]);
    }
}
