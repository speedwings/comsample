<?php

namespace ComHub\Http\Controllers\Import;

use Illuminate\Http\Request;
use ComHub\Http\Controllers\Controller;
use ComHub\Contact;
use Illuminate\Database\Eloquent\Model;

class ImportContactsController extends Controller
{
    protected $transformer;
    protected $table;
    protected $primaryKey;
    protected $skip;
    protected $limit;

    public function __construct($table, $transformer, $connection, $skip, $limit) {
        $class = "\ComHub\Transformers\Import\\$transformer";
        $this->transformer = new $class;
        $this->primaryKey = $this->transformer->getPrimaryKey();
        $this->table = $table;
        $this->skip = $skip;
        $this->limit = $limit;
    }

    public function count() {
        return \DB::table($this->table)->count();
    }

    public function listKeys() {
        return \DB::table($this->table)
                    ->when($this->skip, function($query) {
                        return $query->skip($this->skip);
                    })
                    ->when($this->limit, function ($query) {
                        return $query->limit($this->limit);
                    })
                    ->orderBy('id')
                    ->pluck($this->primaryKey);
    }

    public function import($id) {
        if ( $row = \DB::table($this->table)->where($this->primaryKey, $id)->first()) {
            Model::unguard();
            if ($this->transformer->isActive($row)) {
                Contact::create($this->transformer->transform($row));
            }
            Model::reguard();
        } else {
            throw new \Exception("No item with id $id");
        }
    }
}
