<?php

namespace ComHub\Http\Controllers\Api;

use Illuminate\Http\Request;
use ComHub\Http\Controllers\Controller;

abstract class RestController extends Controller
{
    protected $perPage = 15;
}
