<?php

namespace ComHub\Http\Controllers\Api;

use ComHub\Communication;
use ComHub\Credit;
use Illuminate\Http\Request;
use ComHub\Http\Controllers\Controller;

class CreditsRestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function balance()
    {
    	return Credit::sum('amount') / 100;
    }

	public function checkCredit($amount)
	{
		return $this->balance() >= $amount;
	}

	public function chargeCredit($amount, $accreditable = null)
    {
        if ($this->checkCredit($amount)) {
            $description = ($accreditable) ? $accreditable->getCreditLabel() : 'Accredito';
            $charge = Credit::create([
                'description' => $description,
                'amount' => $amount * -1
            ]);
            $accreditable->credits()->attach($charge->id);
            return true;
        }
        return false;
    }
}
