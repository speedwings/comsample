<?php

namespace ComHub\Http\Controllers\Api;

use Illuminate\Http\Request;
use ComHub\Http\Controllers\Controller;
use DB;

class DataRestController extends Controller
{
    public function list(Request $request, $model, $attribute)
    {
      return DB::table($model)
                ->select($attribute)
                ->whereNotNull($attribute)
                ->groupBy($attribute)
                ->orderBy($attribute)
                ->when($request->has('search'), function($query) use ($request, $attribute) {
                  return $query->where($attribute, 'LIKE', '%'.$request->get('search').'%');
                })->paginate(($request->has('per_page')) ? $request->get('per_page') : 10)->pluck($attribute)->toArray();
    }
}
