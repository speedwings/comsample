<?php

    namespace ComHub\Http\Controllers\Api\Services;

    use ComHub\Http\Controllers\Api\RestController;
    use ComHub\Services\ComuneIstat;
    use Illuminate\Http\Request;

    class ComuniIstatController extends RestController
    {
        public function index(Request $request, $id = null)
        {
            if (!empty($id)) {
                return ComuneIstat::findOrFail($id);
            } else {
                return ComuneIstat::when($request->has('search'), function ($query) use ($request) {
                        $string = $request->get('search');

                        return $query->where('denominazione_corrente', 'like', "$string%");
                    })
                    ->orderBy('denominazione_corrente')
                    ->paginate($request->has('per_page') ? $request->get('per_page') : $this->perPage);
            }
        }
    }
