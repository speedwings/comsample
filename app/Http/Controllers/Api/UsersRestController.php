<?php

namespace ComHub\Http\Controllers\Api;

use ComHub\Filters\UserFilters;
use ComHub\User;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class UsersRestController extends RestController
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index(Request $request, UserFilters $filters)
	{
		return User::filter($filters)->paginate($request->has('per_page') ? $request->get('per_page') : $this->perPage );
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		$this->validate($request, [
			'name' => 'nullable|string|max:191',
			'email' => 'required|string|email|max:191|unique:users',
			'password' => 'required|string|min:6|confirmed',
		]);
		$user = User::make($request->all());
		$user->name = $request->get('name', $request->get('email'));
		$user->password = bcrypt($request->get('password'));
		$user->save();
		return $user;
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		return User::findOrFail($id);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		$rules = [
			'name' => 'nullable|string|max:191',
			'email' => [
				'required',
				'string',
				'email',
				'max:191',
				Rule::unique('users')->where(function ($query) use ($id) {
					return $query->where('id', '!=', $id )->whereNull('deleted_at');
				}),
			]
		];

		if ($request->has('password')) {
			$rules['password'] =  'required|string|min:6|confirmed';
		}
		$this->validate($request, $rules);
		$user = User::findOrFail($id);
		$user->name = $request->get('name', $request->get('email'));
		$user->email = $request->get('email');
		if ($request->has('password')) $user->password = bcrypt($request->get('password'));
		$user->save();
		return $user;
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		User::destroy($id);
	}
}
