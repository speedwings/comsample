<?php

namespace ComHub\Http\Controllers\Api;

use ComHub\Contact;
use ComHub\Filters\ContactFilters;
use ComHub\Filters\SegmentFilters;
use ComHub\Segment;
use Illuminate\Http\Request;

class SegmentsRestController extends RestController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, SegmentFilters $filters)
    {
	    return Segment::filter($filters)->orderBy('description')->paginate($request->has('per_page') ? $request->get('per_page') : $this->perPage );

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return Segment::create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Segment::findOrFail($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $segment = Segment::findOrFail($id);
        if ($segment->update($request->all())) return $segment;
        return response()->json(500, trans('comhub.update_error'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Segment::destroy($id);
    }

    public function count(Request $request, $id = null)
    {
		$filters = new ContactFilters(
			$request,
			($id) ? Segment::findOrFail($id)->filters : null
		);
		return Contact::filter($filters)->count();
    }
}
