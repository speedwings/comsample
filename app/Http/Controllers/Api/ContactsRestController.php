<?php

namespace ComHub\Http\Controllers\Api;

use Carbon\Carbon;
use Illuminate\Http\Request;
use ComHub\Contact;
use Illuminate\Validation\Rule;
use ComHub\Filters\ContactFilters;

class ContactsRestController extends RestController {

	public function __construct() {
		$this->middleware( function( $request, $next ) {
			return $next( $this->sanitizePhone( $request ) );
		} )->only( [ 'store', 'update' ] );
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index( Request $request, ContactFilters $filters ) {
		return Contact::owned()->with( [ 'city' ] )->withCount( [ 'communications' ] )->filter( $filters )->paginate( $request->has( 'per_page' ) ? $request->get( 'per_page' ) : $this->perPage );
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function store( Request $request ) {
		$this->validate( $request, [
			'email'     => [
				Rule::unique( 'contacts' )->where( function( $query ) {
					return $query->whereNotNull( 'email' )->whereNull( 'deleted_at' );
				} ),
				'nullable',
				'email',
				'required_if:email_is_subscribed,true'
			],
			'phone'     => [
				Rule::unique( 'contacts' )->where( function( $query ) {
					return $query->whereNotNull( 'phone' )->whereNull( 'deleted_at' );
				} ),
				'nullable',
				'phone:IT',
				'required_if:phone_is_subscribed,true'
			],
			'birthdate' => 'nullable|date_format:d/m/Y',
			'privacy' => 'required|accepted',
		] );
		if ( $request->has( 'email' ) ) {
			$request->request->add( [ 'email_confirmed_at' => Carbon::now() ] );
		}

		return Contact::create( $request->all() );
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function show( $id ) {
		return Contact::owned()->with( [ 'city', 'communications' ] )->findOrFail( $id );
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function update( Request $request, $id ) {
		$this->validate( $request, [
			'email'     => [
				Rule::unique( 'contacts' )->where( function( $query ) use ( $id ) {
					return $query->where( 'id', '!=', $id )->whereNotNull( 'email' )->whereNotNull( 'email_confirmed_at' )->whereNull( 'deleted_at' );
				} ),
				'nullable',
				'email',
				'required_if:email_is_subscribed,true'
			],
			'phone'     => [
				Rule::unique( 'contacts' )->where( function( $query ) use ( $id ) {
					return $query->where( 'id', '!=', $id )->whereNotNull( 'phone' )->whereNotNull( 'phone_confirmed_at' )->whereNull( 'deleted_at' );
				} ),
				'nullable',
				'phone:IT',
				'required_if:phone_is_subscribed,true'
			],
			'birthdate' => 'nullable|date_format:d/m/Y'
		] );
		$contact = Contact::findOrFail( $id );
//        if ($request->has('email') && !$contact->email_confirmed_at) {
//            $request->request->add(['email_confirmed_at' => Carbon::now()]);
//        }
		$contact->update( $request->all() );

		return $contact;
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function destroy( $id ) {
		Contact::destroy( $id );
	}

	private function sanitizePhone( Request $request ) {
		if ( ! $request->has( 'phone' ) ) {
			return $request;
		}

		if ( ! empty( $request->get( 'phone' ) ) ) {
			$phone = (string) phone( $request->get( 'phone' ), 'IT' );
			$request->merge( [ 'phone' => $phone ] );
		}

		return $request;
	}

	private function forceEmailConfirm() {

	}
}
