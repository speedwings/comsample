<?php

    namespace ComHub\Http\Controllers\Api;

    use Carbon\Carbon;
    use ComHub\Contact;
    use ComHub\Filters\ContactFilters;
    use ComHub\Jobs\MassEmailNotification;
    use ComHub\Jobs\MassSmsNotification;
    use Illuminate\Http\Request;
    use ComHub\Communication;

    class CommunicationsRestController extends RestController
    {

        const DEFAULT_CHUNK = 100;

        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function index(Request $request)
        {
            return Communication::
            with(['type'])->
            withCount(['contacts'])->
            when($request->get('order_by'), function ($query) use ($request) {
                return $query->orderBy($request->get('order_by'), $request->get('order', 'asc'));
            }, function ($query) {
            	return $query->orderBy('created_at', 'DES');
            })
            ->when($request->get('search'), function ($query) use ($request) {
                $search = $request->get('search');
                return $query->where(function ($query) use ($search) {
                    return $query->where('subject', 'LIKE', "%$search%")
                        ->orWhere('body', 'LIKE', "%$search%");
                });
            })
            ->paginate($request->has('per_page') ? $request->get('per_page') : $this->perPage);
        }

        /**
         * Store a newly created resource in storage.
         *
         * @param  \Illuminate\Http\Request $request
         * @return \Illuminate\Http\Response
         */
        public function store(Request $request)
        {
        	$this->validate($request, [
        		'title' => [
        			'required_without:subject'
		        ],
		        'body' => [
			        'required'
		        ],
	        ]);
	        $this->checkFilters($request);
            $communication = Communication::create($request->all());
			$this->saveContacts($communication);

            return $communication;
        }

        /**
         * Display the specified resource.
         *
         * @param  int $id
         * @return \Illuminate\Http\Response
         */
        public function show($id)
        {
            return Communication::with(['type'])->findOrFail($id);
        }

        /**
         * Update the specified resource in storage.
         *
         * @param  \Illuminate\Http\Request $request
         * @param  int $id
         * @return \Illuminate\Http\Response
         */
        public function update(Request $request, $id)
        {
	        $this->validate($request, [
		        'title' => [
			        'required_without:subject'
		        ],
		        'body' => [
		        	'required'
		        ],
	        ]);
	        $this->checkFilters($request);
            $communication = Communication::findOrFail($id);
            $communication->update($request->all());
	        $this->saveContacts($communication);
            return $communication;
        }

        /**
         * Remove the specified resource from storage.
         *
         * @param  int $id
         * @return \Illuminate\Http\Response
         */
        public function destroy($id)
        {
            Communication::destroy($id);
        }

        public function copy($id)
        {
            $old = Communication::findOrFail($id);
            $copy = Communication::create($old->toArray());
            $this->saveContacts($copy);
            return route('communications.edit', $copy->id);
        }

        protected function saveContacts(Communication $communication)
        {
			$contactsFilters = ($communication->segment) ? $communication->segment->filters : $communication->filters;
			$routeFilters = [$communication->type->route.'_state' => 'subscribed'];
			$filters = new ContactFilters(request(), array_merge($contactsFilters, $routeFilters));
			$contacts = Contact::filter($filters)->pluck('id')->toArray();
			$communication->contacts()->sync($contacts);

        }

        public function send($id)
        {
            $communication = Communication::findOrFail($id);

			if ((new CreditsRestController)->chargeCredit($communication->getCost(), $communication)) {
				switch ($communication->type->route) {
					case 'phone':
						return $this->sendText($communication);
					case 'email':
						return $this->sendEmail($communication);
				}
			} else {
				return response()->json(['message' => trans('comhub.insufficient_credits')], 400);
			}
        }

        private function sendText(Communication $communication)
        {
            if ($this->setRequest($communication)) {
                MassSmsNotification::dispatch($communication);
                return route('communications.show', $communication->id);
            } else {
                abort(500, 'Error setting notification sent.');
            };
        }

        private function sendEmail(Communication $communication)
        {
			if ($this->setRequest($communication)) {
				MassEmailNotification::dispatch($communication);
				return route('communications.show', $communication->id);
			} else {
				abort(500, 'Error setting notification sent.');
			}
        }

        private function setRequest(Communication $communication)
        {
            $communication->sent_request_at = Carbon::now();
            $communication->sent_error = null;
            return !!$communication->save();
        }

        private function checkFilters(Request $request) {
        	if (!empty($request->get('segment_id', null))) {
        		$request->merge(['filters' => null]);
	        }
        }

        private function getNormalizedChunk(Communication $communication)
        {
            return CommunicationsRestController::DEFAULT_CHUNK - (CommunicationsRestController::DEFAULT_CHUNK % $communication->type->quantity);
        }
    }
