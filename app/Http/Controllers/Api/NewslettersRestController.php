<?php

namespace ComHub\Http\Controllers\Api;

use Illuminate\Http\Request;
use ComHub\Http\Controllers\Controller;
use ComHub\Contact;
use ComHub\Mail\EmailConfirmation;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Carbon\Carbon;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Mail;

class NewslettersRestController extends Controller
{
    /**
     * Token identified Contact
     *
     * @var \ComHub\Contact
     */
    private $contact;

    public function __construct()
    {
        $this->middleware(function($request, $next) {
            return $this->verificationMiddleware($request, $next);
        })->only([
            'subscribe',
            'unsubscribe',
            'confirm',
            'verify'
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'email' => [
                'required',
                'email',
                'unique_confirmed_or_unsubscribed:contacts',
            ],
            'phone' => [
                'phone:IT',
                'unique_confirmed_or_unsubscribed:contacts',
            ],
            'privacy' => [
                'required',
                'accepted',
            ]
        ]);
	    $field = ($request->has('email')) ? 'email' : 'phone';
	    if (!$contact = Contact::where($field, $request->get($field))->whereNotNull($field.'_confirmed_at')->whereNotNull($field.'_unsubscribed_at')->first()) {
		    $contact = Contact::create(array_merge($request->all(), [
			    'email_subscribed_at' => Carbon::now(),
			    'phone_subscribed_at' => Carbon::now()
		    ]));
	    }
        if (!empty($contact->email) && (empty($contact->email_confirmed_at) || !empty($contact->email_unsubscribed_at))) {
            $url = implode("/", [
                trim(config( 'comhub.frontendUri' ), "/"),
                localize_url(trim(config( 'comhub.confirmUri' ), "/"), app()->getLocale()),
                $contact->email_token
            ]);
            Mail::to( $contact->email )->queue(
                new EmailConfirmation(
                    $contact->email,
                    $url,
                    app()->getLocale()
                )
            );
        }
        return response()->json([
            'message' => trans('newsletter.store_success'),
            'data' => $contact->only(['id', 'email', 'email_token', 'phone', 'phone_token', 'email_confirmed_at', 'phone_confirmed_at'])
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'email' => [
              'email',
              'unique_confirmed:contacts,email_confirmed_at,'.$id
            ],
//            'email_token' => 'required',
            // TODO: Test rules
//            'firstname' => 'required',
//            'lastname' => 'required',
//            'gender' => 'required',
            'phone' => [
              'nullable',
              'phone:IT',
              'unique_confirmed:contacts,phone_confirmed_at,'.$id,
            ],
//            'address' => 'required',
//            'city' => 'required',
//            'district' => 'required',
//            'area' => 'required',
            'birthdate' => 'nullable|date_format:d/m/Y'
        ]);
        if ( $contact = Contact::where('email', $request->get('email'))->where('email_token', $request->get('email_token'))->find($id) ) {
	        if ( $contact->update($request->except([ 'id', 'email', 'email_token' ])) ) {
		        return response()->json([
                    'message' => trans('newsletter.update_success'),
                ], 200);
            } else {
                return response()->json([
                    'message' => trans('newsletter.server_error'),
                ], 500);
            }
        }
        return response()->json([
            'message' => trans('newsletter.contact_not_found'),
        ], 404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function subscribe(Request $request, $type)
    {
        $subscriptionField = $type.'_subscribed_at';
        $unsubscriptionField = $type.'_unsubscribed_at';

        if ($this->contact->email_is_subscribed) return response()->json([
                'message' => trans('newsletter.subscription_success'),
            ], 200);

        if ($this->contact->update([
            $subscriptionField => ($this->contact->email_is_subscribed) ? $this->contact->$subscriptionField : Carbon::now(),
            $unsubscriptionField => null
        ])) {
            return response()->json([
                'message' => trans('newsletter.subscription_success'),
            ], 200);
        }
        return response()->json([
            'message' => trans('newsletter.server_error'),
        ], 500);
    }

    public function unsubscribe(Request $request, $type)
    {
        $unsubscriptionField = $type.'_unsubscribed_at';
        $subscriptionField = $type.'_subscribed_at';

        if (!$this->contact->email_is_subscribed) return response()->json([
                'message' => trans('newsletter.unsubscription_success'),
            ], 200);

        if ($this->contact->update([
            $unsubscriptionField => (!$this->contact->$subscriptionField) ? $this->contact->$unsubscriptionField : Carbon::now()
        ])) {
            return response()->json([
                'message' => trans('newsletter.unsubscription_success'),
            ], 200);
        }
        return response()->json([
            'message' => trans('newsletter.server_error'),
        ], 500);
    }

    public function confirm(Request $request, $type)
    {
        $confirmDateField = $type.'_confirmed_at';
	    $subscriptionField = $type.'_subscribed_at';
	    $unsubscriptionField = $type.'_unsubscribed_at';
	    $subscriptionFlag = $type.'_is_subscribed';

        if ($this->contact->$confirmDateField != null && $this->contact->$subscriptionFlag) {
            return response()->json([
                'message' => trans('newsletter.already_confirmed'),
            ], 200);
        }
        if ($this->contact->update([
            $confirmDateField => (!empty($this->contact->$confirmDateField) && $this->contact->$subscriptionFlag) ? $this->contact->$confirmDateField : Carbon::now(),
            $subscriptionField => (!empty($this->contact->$subscriptionField) && $this->contact->$subscriptionFlag) ? $this->contact->$subscriptionField : Carbon::now(),
            $unsubscriptionField => null
        ])) {
        	$pending = Contact::where($type, $this->contact->$type)->whereNull($confirmDateField)->pluck('id')->toArray();
        	Contact::destroy($pending);
            return response()->json([
                'message' => trans('newsletter.confirm_success'),
            ], 200);
        }
        return response()->json([
            'message' => trans('newsletter.server_error'),
        ], 500);
    }

    public function verify(Request $request, $type)
    {
        $confirmDateField = $type . '_confirmed_at';
        $subscriptionField = $type . '_subscribed_at';
        $unsubscriptionField = $type . '_unsubscribed_at';
        $subscriptionFlag = $type . '_is_subscribed';
        return [
            'firstname' => $this->contact->firstname,
            'lastname' => $this->contact->lastname,
            $type => $this->contact->$type,
            $confirmDateField => ($this->contact->$confirmDateField) ? $this->contact->$confirmDateField->format('d/m/Y') : null,
            $subscriptionField => ($this->contact->$subscriptionField) ? $this->contact->$subscriptionField->format('d/m/Y') : null,
            $unsubscriptionField => ($this->contact->$unsubscriptionField) ? $this->contact->$unsubscriptionField->format('d/m/Y') : null,
            $subscriptionFlag => $this->contact->$subscriptionFlag
        ];
    }

    protected function verificationMiddleware(Request $request, $next)
    {
        if (!$request->has('token')) abort(403, trans('newsletter.wrong_or_empty_token'));

        $type = $request->route('type');

        if ($type != 'email' && $type != 'phone') abort(404, trans('newsletter.invalid_type'));

        if ($contact = Contact::where($type.'_token', $request->get('token'))->first()) {
            $this->contact = $contact;
        } else {
            abort(403, trans('newsletter.wrong_or_empty_token'));
        }

        return $next($request);
    }
}
