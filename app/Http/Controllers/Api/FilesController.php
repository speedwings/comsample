<?php

namespace ComHub\Http\Controllers\Api;

use Illuminate\Http\Request;
use ComHub\Http\Controllers\Controller;
use Storage;
use Image;
use Intervention\Image\Exception\NotFoundException;

class FilesController extends Controller
{

    public function uploadPictures(Request $request)
    {
        $width = $request->get('width', null);
        $height = $request->get('height', null);
        $paths = [];
        foreach ($request->file('files') as $file) {
            $filename = $file->hashName('images');
            $path = Storage::disk('public')->path($filename);
            Image::make($file->getRealPath())->resize($width, $height, function ($constraint) {
                $constraint->aspectRatio();
                // $constraint->upsize();
            })->save($path);
            $paths[] = $filename;
            // $paths[] = $file->store('images', 'public');
        }
        return response()->json(['message' => 'uploaded', 'status_code' => 200, 'data' => [
            'paths' => $paths
        ]]);
    }

    public function deletePicture(Request $request)
    {
        $path = $request->get('path');
        if (Storage::disk('public')->exists($path)) {
            Storage::disk('public')->delete($path);
            return response()->json([
                'message' => 'File deletion completed',
                'status_code' => 200
            ]);
        } else {
            return response()->json([
                'message' => 'The file requested do not exists',
                'status_code' => 403
            ], 403);
        }
    }
}
