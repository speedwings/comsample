<?php

namespace ComHub\Http\Middleware;

use Closure;

class HasRole
{
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next, $role)
	{
		if (! $request->user()->hasRole($role)) {
			if ($request->ajax()) return response()->json(['message' => 'Unauthorized'], 401);
			return redirect(route('home'));
		}
		return $next($request);
	}
}
