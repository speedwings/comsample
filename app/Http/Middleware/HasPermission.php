<?php

namespace ComHub\Http\Middleware;

use Closure;

class HasPermission
{
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next, $permission)
	{
		if (! $request->user()->hasPermission($permission)) {
			if ($request->ajax()) return response()->json(['message' => 'Unauthorized'], 401);
			return redirect(route('home'));
		}
		return $next($request);
	}
}
