<?php

namespace ComHub\Transformers\Import;

class BaseContactsTransformer {

  protected $primaryKey = 'id';

  public function transform($data) {
    return $data;
  }

  public function isActive($data) {
    return true;
  }

  public function getPrimaryKey()
  {
    return $this->primaryKey;
  }

}