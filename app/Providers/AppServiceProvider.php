<?php

namespace ComHub\Providers;

use Carbon\Carbon;
use ComHub\Communication;
use ComHub\Jobs\MassEmailNotification;
use ComHub\Notifications\Channels\SkebbySmsChannel;
use ComHub\Notifications\Clients\SkebbyClient;
use ComHub\Notifications\Clients\SparkPostClient;
use ComHub\Observers\CommunicationObserver;
use ComHub\Observers\SegmentObserver;
use ComHub\Segment;
use Illuminate\Mail\Transport\SparkPostTransport;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use ComHub\Observers\ContactObserver;
use ComHub\Contact;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        Contact::observe(ContactObserver::class);
        Communication::observe(CommunicationObserver::class);
        Segment::observe(SegmentObserver::class);

        \Validator::extend('date_multiformat', function($attribute, $value, $formats) {
          if (empty($value)) return true;
          foreach($formats as $format) {
            $parsed = date_parse_from_format($format, $value);
            if ($parsed['error_count'] === 0 && $parsed['warning_count'] === 0) {
              return true;
            }
          }
          return false;
        });

        \Validator::extend('unique_confirmed', function($attribute, $value, $args) {
          if (empty($value) || empty($args)) return true;
          if ($attribute === 'phone') $value = phone($value, 'IT');
          $table = $args[0];
          $confirmField = (isset($args[1])) ? $args[1] : $attribute.'_confirmed_at';
          $query = \DB::table($table)->where($attribute, $value)->whereNotNull($confirmField);
          $query = (isset($args[2])) ? $query->where('id', '!=', $args[2]) : $query;
          try {
          	$count = $query->whereNull('deleted_at')->count();
          } catch (\Exception $e) {
            $count = $query->count();
          }
          return !$count;
        });

	    \Validator::extend('unique_confirmed_or_unsubscribed', function($attribute, $value, $args) {
		    if (empty($value) || empty($args)) return true;
		    if ($attribute === 'phone') $value = phone($value, 'IT');
		    $table = $args[0];
		    $confirmField = (isset($args[1])) ? $args[1] : $attribute.'_confirmed_at';
		    $unsubscribedField = (isset($args[2])) ? $args[2] : $attribute.'_unsubscribed_at';
		    $query = \DB::table($table)->where($attribute, $value)->where(function($query) use ($confirmField, $unsubscribedField) {
		    	return $query->whereNotNull($confirmField)->whereNull($unsubscribedField);
		    });
		    $query = (isset($args[3])) ? $query->where('id', '!=', $args[3]) : $query;
		    try {
			    $count = $query->whereNull('deleted_at')->count();
		    } catch (\Exception $e) {
			    $count = $query->count();
		    }
		    return !$count;
	    });

	    \Blade::if('role', function($role) {
		    $user = \Auth::user();
		    if (!$user) return false;
		    return $user->hasRole($role);
	    });

	    \Blade::if('permission', function($permission) {
		    $user = \Auth::user();
		    if (!$user) return false;
		    return $user->hasPermission($permission);
	    });

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        setLocale(LC_TIME, 'it_IT');
        Carbon::setLocale('it');

        config(['app.defaultLocale' => config('app.locale')]);

        $this->app->bind(SkebbyClient::class, function() {
	        return new SkebbyClient(
				config('services.skebby.gateway'),
				config('services.skebby.username'),
				config('services.skebby.password'),
				config('services.skebby.sender'),
				config('services.skebby.debug'),
				config('services.skebby.debug_request'),
				config('services.skebby.pretend')
	        );
        });

        $this->app->bind(SparkPostClient::class, function() {
        	return new SparkPostClient([
        		'open_tracking' => true,
		        'click_tracking' => true,
		        'transactional' => false,
	        ]);
        });
    }
}
