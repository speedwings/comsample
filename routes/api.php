<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'auth:api', 'as' => 'api.'], function() {
    // Contacts routes
    Route::apiResource('contacts', 'Api\ContactsRestController');

	// Users routes
	Route::apiResource('users', 'Api\UsersRestController')->middleware('permission:edit_users');

	// Communications routes
    Route::apiResource('communications', 'Api\CommunicationsRestController');
    Route::get('communications/{id}/copy', 'Api\CommunicationsRestController@copy')->name('communications.copy');
    Route::put('communications/{id}/send', 'Api\CommunicationsRestController@send')->name('communications.send');

    // Communication types routes
	Route::apiResource('communication_types', 'Api\CommunicationTypesRestController');

	// Credits routes
	Route::apiResource('credits', 'Api\CreditsRestController');
	Route::get('credits', 'Api\CreditsRestController@balance')->name('credits.balance');

	// Segments routes
	Route::get('segments/count/{id?}', 'Api\SegmentsRestController@count')->name('segments.count');
	Route::apiResource('segments', 'Api\SegmentsRestController');

	// Settings routes
	Route::get('settings/{setting}/{value}', 'Api\SettingsRestController@value')->name('settings.show.value');
	Route::apiResource('settings', 'Api\SettingsRestController');

    // Pictures Routes
    Route::post('pictures', 'Api\FilesController@uploadPictures')->name('pictures.store');
    Route::delete('pictures', 'Api\FilesController@deletePicture')->name('pictures.destroy');

    // API Data
    Route::get('data/comuni/{id?}', 'Api\Services\ComuniIstatController@index')->name('comuni_istat');
    Route::get('data/{model}/{attribute}', 'Api\DataRestController@list')->name('model_data');
});

// Newsletter
Route::group([], function() {
    Route::group(['prefix' => 'newsletters'], function() {
        Route::post('{type}/subscribe', 'Api\NewslettersRestController@subscribe');
        Route::post('{type}/unsubscribe', 'Api\NewslettersRestController@unsubscribe');
        Route::post('{type}/confirm', 'Api\NewslettersRestController@confirm');
        Route::post('{type}/verify', 'Api\NewslettersRestController@verify');
    });
    Route::apiResource('newsletters', 'Api\NewslettersRestController', ['as' => 'api'])->middleware('client');
});

Route::group(['prefix' => 'services', 'as' => 'api.services.', 'middleware' => ['client']], function() {
	Route::get('comuni/{id?}', 'Api\Services\ComuniIstatController@index')->name('comuni_istat');
});
