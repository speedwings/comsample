<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

$this->get('pinfo', function() {
	echo phpinfo();
});

// Auth::routes();
// Authentication Routes...
$this->get('login', 'Auth\LoginController@showLoginForm')->name('login');
$this->post('login', 'Auth\LoginController@login');
$this->post('logout', 'Auth\LoginController@logout')->name('logout');

// Registration Routes...
// $this->get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
// $this->post('register', 'Auth\RegisterController@register');

// Password Reset Routes...
$this->get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
$this->post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
$this->get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
$this->post('password/reset', 'Auth\ResetPasswordController@reset');

Route::get('news/{id}', 'MailPreviewController@html')->name('news.html');

Route::group(['middleware' => 'auth'], function() {
    Route::get('/', 'HomeController@index')->name('home');
    Route::resource('contacts', 'ContactsController', ['only' => ['index', 'edit', 'create']]);
    Route::resource('communications', 'CommunicationsController', ['except' => ['store', 'update', 'destroy']]);
    Route::get('communications/create/{type}', 'CommunicationsController@create')->name('communications.create_type');
	Route::resource('users', 'UsersController', ['only' => ['index', 'edit', 'create']])->middleware('permission:edit_users');
});

Route::group(['prefix' => 'export'], function() {
    Route::get('contacts/{format}', 'ExportController@exportContacts');
});

Route::group(['prefix' => 'debug', 'middleware' => 'auth'], function() {
    Route::Get('mailable', function(\Illuminate\Http\Request $request) {
        $email = $request->get('email', env('ADMIN_EMAIL'));
        $url = $request->get('url', 'http://localhost');
        return new \ComHub\Mail\EmailConfirmation($email, $url);
    });
});
